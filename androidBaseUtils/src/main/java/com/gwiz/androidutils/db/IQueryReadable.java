package com.gwiz.androidutils.db;



/**
 * Interface specifying object has readable database behavior. 
 *
 */
public interface IQueryReadable {

	/**
	 * Selects data based on specified hard written SQL query.
	 * @param db The Database to query.
	 * @param sqlQuery The string SQL query.
	 * @throws DBException
	 */
	void 	select(Database db, String sqlQuery) throws DBException;
	
	/**
	 * Selects data based on loaded record fields.
	 * @param db The Database to query.
	 * @param keys The number of keys in column order to select on. The object must extend using the AQueryRecord class and 
	 * specify the fields @Override the BindData() method.
	 * @throws DBException
	 */
	void 	select(Database db, int keys) throws DBException;
}

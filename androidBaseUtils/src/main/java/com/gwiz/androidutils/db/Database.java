package com.gwiz.androidutils.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Abstract database with basic functionality wrapped over 
 * {@link android.database.sqlite.SQLiteOpenHelper SQLiteOpenHelper} 
 * making it easier to prototype out child databases.
 * <p>
 * 
 * @author pgalasti@gmail.com
 */
public abstract class Database extends SQLiteOpenHelper {

	protected List<IRecordAttributes> viewList;

	/**
	 * Public constructor for the database object. 
	 * @param context Application context.
	 * @param name Database name.
	 * @param version Current version of the database.
	 */
	public Database(Context context, String name, int version) {
		super(context, name, null, version);
		
		viewList = new ArrayList<IRecordAttributes>();
		setViewList();
	}

	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		for(Iterator<IRecordAttributes> iterator = viewList.iterator(); iterator.hasNext();)
		{
			IRecordAttributes currentRecord = iterator.next();
			db.execSQL(currentRecord.getSQLCreateStatement());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		for(Iterator<IRecordAttributes> iterator = viewList.iterator(); iterator.hasNext();)
		{
			IRecordAttributes currentRecord = iterator.next();
			String alterQuery = currentRecord.getSQLAlterSchema(oldVersion, newVersion);
			if( !alterQuery.isEmpty() )
				db.execSQL(alterQuery);
		}
		
	}

    /**
     * Starts a database transaction.
     */
    public void startTransaction() {
        this.getWritableDatabase().beginTransaction();
    }

    /**
     * Finishes a database transaction. If the transaction fails, the database state rolls back
     * prior to startTransaction().
     */
    public void finishTransaction() {
        try {
            this.getWritableDatabase().setTransactionSuccessful();
        } catch (Exception e) {
            throw new RuntimeException("Unable to finish transaction!");
        } finally {
            this.endTransactionWithoutSaving();
        }
    }

    /**
     * Rolls the database state back prior to startTransaction().
     */
    public void endTransactionWithoutSaving() {
        this.getWritableDatabase().endTransaction();
    }

	/**
	 * Child specific tables should be defined and added to the viewList for all
	 * creation and alterations managed through the {@link com.gwiz.androidutils.db Database}.
	 */
	protected abstract void setViewList();


	/**
	 * Returns the database name.
	 */
	public abstract String getDatabaseName();
	
	/**
	 * Returns the DBMS name.
	 * @return The DBMS name.
	 */
	public abstract String getDBMSName();
	
	/**
	 * Returns the DBMS Version.
	 * @return The DBMS Version.
	 */
	public abstract String getDBMSVersion();
	
}

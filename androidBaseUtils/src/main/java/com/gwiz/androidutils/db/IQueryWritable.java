package com.gwiz.androidutils.db;

import android.database.sqlite.SQLiteDatabase;

/**
 * Interface specifying object has writable database behavior. 
 *
 */
public interface IQueryWritable {

	/**
	 * Update the in memory data fields to the database. The object must extend using the AQueryRecord class and 
	 * specify the fields @Override the BindData() method.
	 * @param db The Database to perform the update on.
	 * @return The number of rows updated.
	 */
	int		update(Database db) throws DBException;
	
	/**
	 * Insert the in memory data fields to the database. The object must extend using the AQueryRecord class and 
	 * specify the fields @Override the BindData() method.
	 * @param db The Database to perform the insert on.
	 * @return The row ID of the inserted row.
	 */
	long	insert(Database db) throws DBException;
	
	/**
	 * Delete the in memory data fields to the database. The object must extend using the AQueryRecord class and 
	 * specify the fields @Override the BindData() method.
	 * @param db The Database to perform the deletion on.
	 * @return The number of rows deleted.
	 */
	int 	delete(Database db) throws DBException;
}

package com.gwiz.androidutils.string;

import java.util.ArrayList;

/**
 * Simple line parser for parsing tokens using a {@link java.lang.Character char} delimiter.
 * @author pgalasti@gmail.com
 *
 */
public class LineParser {

	private ArrayList<String> tokens = new ArrayList<String>();
	private String line;
	private char delimiter;
	
	/**
	 * Full constructor accepting a line and delimiter.
	 * <p>
	 * 
	 * @param line The line to be parsed.
	 * @param delimiter The delimiter to parse the string with.
	 */
	
	public LineParser(final String line, final char delimiter)
	{
		setLineDelimiter(line, delimiter);
	}
	
	/**
	 * Blank constructor setting a blank line and a white space (dec 32) 0x0020 
	 * as the delimiter
	 * <p>
	 */
	public LineParser()
	{
		this("", ' ');
	}
	
	/**
	 * Loads a new {@link java.lang.String string} line and 
	 * {@link java.lang.Character char} delimiter into the parser.
	 * @param line New string line.
	 * @param delimiter New delimiter character.
	 */
	public void setLineDelimiter(final String line, final char delimiter)
	{
		setLine(line);
		setDelimiter(delimiter);
	}
	
	/**
	 * Loads a new {@link java.lang.String string} line into the parser.
	 * @param line New string line.
	 */
	public void setLine(final String line)
	{
		this.line = line;
	}
	
	/**
	 * Loads a new {@link java.lang.Character char} delimiter into the parser.
	 * @param delimiter New delimiter character.
	 */
	public void setDelimiter(final char delimiter)
	{
		this.delimiter = delimiter;
	}
	
	/**
	 * Parses the new line {@link java.lang.String string} with the 
	 * new {@link java.lang.Character char} delimiter character.
	 * @param line New string line.
	 * @param delimiter New delimiter character.
	 */
	public void parse(final String line, final char delimiter)
	{
		setLineDelimiter(line, delimiter);
		parse();
	}
	
	/**
	 * Parses the loaded line {@link java.lang.String string} with the 
	 * loaded {@link java.lang.Character char} delimiter character.
	 */
	public void parse()
	{
		if( line.isEmpty() || delimiter == '\u0000')
			return;
		
		int lineLength = line.length();

		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < lineLength; i++)
		{
			char currentCharacter = line.charAt(i);
			if( currentCharacter == delimiter )
			{
				tokens.add(sb.toString());
				sb.delete(0, sb.length());
				continue;
			}
			
			sb.append(currentCharacter);
		}
		tokens.add(sb.toString());
	}
	
	/**
	 * @return The returned parsed token list.
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<String> getTokenList()
	{
		return (ArrayList<String>) tokens.clone();
	}
	
}

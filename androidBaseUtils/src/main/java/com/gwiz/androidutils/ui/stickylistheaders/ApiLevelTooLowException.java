package com.gwiz.androidutils.ui.stickylistheaders;

//All credit to Emil Sjlander sjolander.emil@gmail.com https://github.com/emilsjolander/StickyListHeaders

public class ApiLevelTooLowException extends RuntimeException {

    private static final long serialVersionUID = -5480068364264456757L;

    public ApiLevelTooLowException(int versionCode) {
        super("Requires API level " + versionCode);
    }

}

package com.gwiz.androidutils.ui.enhancements;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class BlockablePager extends ViewPager {

	int _lastIndex = 99;
	boolean _canGoRight = true;
	float _onDownX = 0f;
	
	public void setMaxIndex(int maxIndex) {
		_lastIndex = maxIndex;
	}
	
	public BlockablePager(Context context) {
		super(context);
	}
	
	public BlockablePager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {

		if( _canGoRight )
			return super.onInterceptTouchEvent(event);
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		_canGoRight = true;
		if( _lastIndex <= getCurrentItem() )
			_canGoRight = false;
		
		boolean lockScroll = false;
		switch (event.getAction()) 
		{ 
		case MotionEvent.ACTION_DOWN:
			_onDownX = event.getX();
			return super.onTouchEvent(event);
			
		case MotionEvent.ACTION_MOVE:
			if( _canGoRight )
				lockScroll = false;
			else if (_onDownX < event.getX()) 
				lockScroll = false;
			else 
				lockScroll = true;
			
			break; 
		}  
		 
		if (lockScroll)
			return false; 
		
		return super.onTouchEvent(event);
	}

}

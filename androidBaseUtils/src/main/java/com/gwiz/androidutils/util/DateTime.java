package com.gwiz.androidutils.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Utility class used for wrapping date/time logic in a more simple class. 
 * @author pgalasti@gmail.com
 *
 */
public class DateTime {

	/**	 Displays Date in <b>MM/DD/CCYY</b> string format from display methods. 	 */
	public static final int MMDDCCYY 	= 0x0001;
	/**	 Displays Date in <b>MM/DD/YY</b> string format from display methods. 	 */
	public static final int MMDDYY 		= 0x0002;
	/**	 Displays Date in <b>DD/MM/CCYY</b> string format from display methods. 	 */
	public static final int DDMMCCYY 	= 0x0004;
	/**	 Displays Date in <b>DD/MM/YY</b> string format from display methods. 	 */
	public static final int DDMMYY 		= 0x0008;
	
	/**	 Displays Time in <b>HH:MM:SS:MS</b> string format from display methods. 	 */
	public static final int HHMMSSMS 	= 0x0100;
	/**	 Displays Time in <b>HH:MM:SS</b> string format from display methods. 	 */
	public static final int HHMMSS 		= 0x0200;
	/**	 Displays Time in <b>HH:MM</b> string format from display methods. 	 */
	public static final int HHMM 		= 0x0400;
	
	private Calendar _calendar = Calendar.getInstance();
	
	private long _milliseconds;
	
	private final static long MILLISECONDS_IN_DAY 		= 86400000L;
	
	/**
	 * Returns the difference in days between the two dates. If the difference of days is negative, 
	 * <b>date1</b> is before <b>date2</b>. This method excludes time of day.
	 * 
	 * @param date1 The first date.
	 * @param date2 The second date.
	 * @return The days between <b>date1</b> and <b>date2</b>
	 */
	public static int differenceBetweenDates(DateTime date1, DateTime date2) 
	{
		// Remove time from calculation.
		date1.setTimeToStartOfDay();
		date2.setTimeToStartOfDay();
		
		// Qualify whether date1 is before or after date2. 
		int qualifier = 1;
		if( date1.getTimeInMilliseconds() < date2.getTimeInMilliseconds() )
			qualifier = -1;
		
		int daysBetween = (int)((date1.getTimeInMilliseconds() - date2.getTimeInMilliseconds()) / MILLISECONDS_IN_DAY);
		return daysBetween * qualifier;
	}

    public static boolean isDateBefore(DateTime dateToCheck, DateTime date) {
        int days = differenceBetweenDates(dateToCheck, date);

        if(days < 0) {
            return true;
        }

        return false;
    }

	/**
	 * Default Constructor. The date/time will automatically be set to the GMT time 
	 * the Date object was initialized.
	 */
	public DateTime()
	{
		_milliseconds = System.currentTimeMillis();
		_calendar.set(Calendar.MILLISECOND, (int) _milliseconds);
	}
	
	/**
	 * The date/time will be set according to the millisecond time given.
	 * @param milliseconds The date/time in milliseconds being initalized.
	 */
	public DateTime(long milliseconds)
	{
		setDateTime(milliseconds);
	}
	
	/**
	 * The date/time will be set according to the date/month/year given.
	 * The time of the date will be set as 0 representing the earliest time in the day.
	 * @param day The calendar date.
	 * @param month The calendar month.
	 * @param year The calendar year.
	 */
	public DateTime(int month, int day, int year)
	{
		_calendar.set(Calendar.DAY_OF_MONTH, day);
		_calendar.set(Calendar.MONTH, month-1);
		_calendar.set(Calendar.YEAR, year);
		
		setTimeToStartOfDay();
		
		_milliseconds = _calendar.getTimeInMillis();
	}
	
	/**
	 * The date/time will be set according to the date/month/year:hour/minute/second/millisecond given.
	 * @param month
	 * @param day
	 * @param year
	 * @param hour
	 * @param minute
	 * @param second
	 * @param millisecond
	 */
	public DateTime(int month, int day, int year, int hour, int minute, int second, int millisecond)
	{
		_calendar.set(Calendar.DAY_OF_MONTH, day);
		_calendar.set(Calendar.MONTH, month-1);
		_calendar.set(Calendar.YEAR, year);
		
		_calendar.set(Calendar.HOUR, hour);
		_calendar.set(Calendar.MINUTE, minute);
		_calendar.set(Calendar.SECOND, second);
		_calendar.set(Calendar.MILLISECOND, millisecond);
		
		_milliseconds = _calendar.getTimeInMillis();
	}
	
	/**
	 * Sets the date based on passed milliseconds
	 * @param milliseconds Time to be set in milliseconds
	 */
	public void setDateTime(long milliseconds)
	{
		_milliseconds = milliseconds;
		_calendar.setTimeInMillis(_milliseconds);
	}
	
	/**
	 * Returns the date/time in milliseconds from the epoch.
	 * @return The date/time in milliseconds from the epoch.
	 */
	public long getTimeInMilliseconds() {
		return _milliseconds;
	}
	
	/**
	 * Sets the date/time to the current system time.
	 */
	public void setCurrent() {
		_calendar.setTimeInMillis(System.currentTimeMillis());
	}
	
	/**
	 * Changes the date/time by the number of days passed. This value can be positive or negative
	 * to traverse time. 
	 * @param days The number of days being added or subtracted to the date.
	 */
	public void changeDays(int days)
	{
		_calendar.add(Calendar.DATE, days);
		_milliseconds = _calendar.getTimeInMillis();
	}
	
	/**
	 * Changes the date/time by the number of months passed. This value can be positive
	 * or negative to traverse time.
	 * @param months The number of months being added or subtracted to the date.
	 */
	public void changeMonths(int months)
	{
		_calendar.add(Calendar.MONTH, months);
		_milliseconds = _calendar.getTimeInMillis();
	}
	
	/**
	 * Changes the date/time by the number of years passed. This value can be positive
	 * or negative to traverse time.
	 * @param years The number of years being added or subtracted to the date.
	 */
	public void changeYears(int years)
	{
		_calendar.add(Calendar.YEAR, years);
		_milliseconds = _calendar.getTimeInMillis();
	}
	
	/**
	 * Returns the day of the month. 
	 * @return The day of the month.
	 */
	public int getDayOfMonth() {
		return _calendar.get(Calendar.DATE);
	}
	
	/**
	 * Returns the month of the year.
	 * @return The month of the year.
	 */
	public int getMonth() {
		return _calendar.get(Calendar.MONTH)+1;
	}
	
	/**
	 * Returns the year.
	 * @return The year.
	 */
	public int getYear() {
		return _calendar.get(Calendar.YEAR);
	}
	
	
	/**
	 * Sets the date/time by setting the time to the beginning of the day.
	 * The time of the date will be set as 0 representing the earliest time in the day.
	 */
	public void setTimeToStartOfDay()
	{
		_calendar.set(Calendar.HOUR, 0);
		_calendar.set(Calendar.MINUTE, 0);
		_calendar.set(Calendar.SECOND, 0);
		_calendar.set(Calendar.MILLISECOND, 0);
		
		_milliseconds = _calendar.getTimeInMillis();
	}
	
	public void setTimetoEndOfDay()
	{
		_calendar.set(Calendar.HOUR_OF_DAY, 23);
		_calendar.set(Calendar.MINUTE, 59);
		_calendar.set(Calendar.SECOND, 59);
		_calendar.set(Calendar.MILLISECOND, 999);
		
		_milliseconds = _calendar.getTimeInMillis();
	}
	
	/**
	 * Displays the date in string format. Numerics are separated by the '/' character.
	 * 
	 * @param displayOptions The format of the date.
	 * @return A string representation of just the date.
	 */
	public String dateToString(final int displayOptions)
	{
		StringBuilder sb = new StringBuilder();
		
		if ((MMDDCCYY & displayOptions) == MMDDCCYY)
		{
			sb.append(_calendar.get(Calendar.MONTH)+1).append("/")
			.append(_calendar.get(Calendar.DATE)).append("/")
			.append(_calendar.get(Calendar.YEAR));
		}
		else if ((MMDDYY & displayOptions) == MMDDYY)
		{
			String lastTwoDigits = String.valueOf(_calendar.get(Calendar.YEAR));
			lastTwoDigits = lastTwoDigits.substring(lastTwoDigits.length()-2);
			
			sb.append(_calendar.get(Calendar.MONTH)+1).append("/")
			.append(_calendar.get(Calendar.DATE)).append("/")
			.append(lastTwoDigits);
		}
		else if ((DDMMCCYY & displayOptions) == DDMMCCYY)
		{
			sb.append(_calendar.get(Calendar.DATE)).append("/")
			.append(_calendar.get(Calendar.MONTH)+1).append("/")
			.append(_calendar.get(Calendar.YEAR));
		}
		else if ((DDMMCCYY & displayOptions) == DDMMCCYY)
		{
			String lastTwoDigits = String.valueOf(_calendar.get(Calendar.YEAR));
			lastTwoDigits = lastTwoDigits.substring(lastTwoDigits.length()-2);
			
			sb.append(_calendar.get(Calendar.MONTH)+1).append("/")
			.append(_calendar.get(Calendar.DATE)).append("/")
			.append(lastTwoDigits);
		}
		
		return sb.toString();
	}
	
	/**
	 * Sets the date to the start of the month 01/MM/CCYY
	 */
	public void setToBeginningOfMonth() {
		
		this.setCurrent();
		
		final int DAY_OF_MONTH = this.getDayOfMonth();
		this.changeDays(0-DAY_OF_MONTH+1);
		
		this.setTimeToStartOfDay();
	}
	
	/**
	 * Sets the date to the end of the month
	 */
	public void setToEndOfMonth() {

		this.setToBeginningOfMonth();
		
		this.changeMonths(1);
		this.changeDays(-1);

		this.setTimeToStartOfDay();
	}
	
	public String timeToString(int displayOptions)
	{
		StringBuilder sb = new StringBuilder();
		
		if ((HHMMSSMS & displayOptions) == HHMMSSMS)
		{
			sb.append(_calendar.get(Calendar.HOUR)).append(":")
			.append(_calendar.get(Calendar.MINUTE)).append(":")
			.append(_calendar.get(Calendar.SECOND)).append(":")
			.append(_calendar.get(Calendar.MILLISECOND));
		}
		else if ((HHMMSS & displayOptions) == HHMMSS)
		{
			sb.append(_calendar.get(Calendar.HOUR)).append(":")
			.append(_calendar.get(Calendar.MINUTE)).append(":")
			.append(_calendar.get(Calendar.SECOND)).append(":");
		}
		else if ((HHMM & displayOptions) == HHMM)
		{
			sb.append(_calendar.get(Calendar.HOUR)).append(":")
			.append(_calendar.get(Calendar.MINUTE)).append(":");
		}
		
		return sb.toString();
	}
	
	public String dateTimeToString(int displayOptions)
	{
		StringBuilder sb = new StringBuilder();
		
		sb.append(dateToString(displayOptions)).append(" ")
		.append(timeToString(displayOptions));
		
		return sb.toString();
	}
}


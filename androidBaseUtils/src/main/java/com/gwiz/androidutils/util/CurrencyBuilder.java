package com.gwiz.androidutils.util;

import java.util.Currency;
import java.util.Locale;

/**
 * Created by Paul on 3/10/2015.
 */
public class CurrencyBuilder {

    /**
     * Formats a currency with the default system currency symbol and digits.
     * @param amount Amount to be formatted
     * @return A string with the symbol and digits of the currency
     */
    public static String getCurrencyWithSymbol(float amount) {
        Currency currency = Currency.getInstance(Locale.getDefault());
        String symbol = currency.getSymbol();

        int decimalDigits = currency.getDefaultFractionDigits();
        String strValue = String.format("%s%."+decimalDigits+"f", symbol, amount);
        if(strValue.charAt(1) == '-') {
            strValue = String.format("(%s%."+decimalDigits+"f)", symbol, amount*-1);
        }
        return strValue;
    }

    /**
     * Formats a currency with the default system currency symbol and digits.
     * @param amount Amount to be formatted
     * @return A string with the symbol and digits of the currency
     */
    public static String getCurrencyWithSymbol(double amount) {
        Currency currency = Currency.getInstance(Locale.getDefault());
        String symbol = currency.getSymbol();

        int decimalDigits = currency.getDefaultFractionDigits();
        String strValue = String.format("%s%."+decimalDigits+"f", symbol, amount);
        if(strValue.charAt(1) == '-') {
            strValue = String.format("(%s%."+decimalDigits+"f)", symbol, amount*-1);
        }
        return strValue;
    }
}

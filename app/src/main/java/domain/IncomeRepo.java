package domain;

import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.util.DateTime;
import infrastructure.dao.IncomeDao;
import infrastructure.data.IncomeDto;
import infrastructure.record.IncomeRecord;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by pgalasti on 2/1/16.
 */
public class IncomeRepo extends Repository<Income, String> {


    protected IncomeDao incomeDao;

    public IncomeRepo(Database db) {

        super(db);
        this.incomeDao = new IncomeDao(db);
    }

    @Override
    public Income find(String guid) {
        IncomeRecord incomeRecord = new IncomeRecord();
        incomeRecord.record.incomeGuid = guid;

        incomeRecord.select(this.db, 1);
        return this.extractFromDao(incomeRecord.record);
    }

    @Override
    public void save(Income income) {

        IncomeRecord incomeRecord = new IncomeRecord();
        incomeRecord.record.label = income.getName();
        incomeRecord.record.amount = income.getAmount();
        incomeRecord.record.date = new DateTime(income.getDate().getTimeInMilliseconds());
        incomeRecord.record.scheduledInd = income.isReoccurring() ? 'Y' : 'N';
        if(income.isOccurrenceDaily()) {
            incomeRecord.record.occurrenceInd = IncomeRecord.OCCURRENCE_IND_DAILY;
        } else if(income.isOccurrenceWeekly()) {
            incomeRecord.record.occurrenceInd = IncomeRecord.OCCURRENCE_IND_WEEKLY;
        } else if(income.isOccurrenceBiWeekly()) {
            incomeRecord.record.occurrenceInd = IncomeRecord.OCCURRENCE_IND_BIWEEKLY;
        } else if(income.isOccurrenceMonthly()) {
            incomeRecord.record.occurrenceInd = IncomeRecord.OCCURRENCE_IND_MONTHLY;
        } else if(income.isOccurrenceBiMonthly()) {
            incomeRecord.record.occurrenceInd = IncomeRecord.OCCURRENCE_IND_BIMONTHLY;
        } else if(income.isOccurrenceSpecificDate()) {
            incomeRecord.record.occurrenceInd = IncomeRecord.OCCURRENCE_IND_SPECIFIC;
        } else {
            throw new RuntimeException("Income occurrence type is not set.");
        }

        if(income.guid.isEmpty()) {
            incomeRecord.record.incomeGuid = income.guid = UUID.randomUUID().toString();
            incomeRecord.insert(super.db);
            return;
        }

        incomeRecord.record.incomeGuid = income.guid;
        incomeRecord.update(super.db);
    }

    @Override
    public void delete(Income income) {

        if(income.guid.isEmpty()) {
            throw new RuntimeException("Income key is empty for deletion.");
        }

        IncomeRecord incomeRecord = new IncomeRecord();
        incomeRecord.record.incomeGuid = income.guid;
        incomeRecord.delete(super.db);
    }

    public Income create() {
        return new Income();
    }

    public List<Income> getAllIncomes() {

        List<Income> incomeList = new ArrayList<>();

        final List<IncomeDto> incomeDtoList = this.incomeDao.getAllIncomes();
        for(IncomeDto incomeDto : incomeDtoList) {
            incomeList.add(this.extractFromDao(incomeDto));
        }

        return incomeList;
    }



    private Income extractFromDao(final IncomeDto record) {
        Income income = new Income();

        income.guid = record.incomeGuid;
        income.name = record.label;
        income.amount = record.amount;
        income.reoccurrence = record.scheduledInd == 'Y';
        income.date = new DateTime(record.date.getTimeInMilliseconds());
        switch (record.occurrenceInd) {
            case IncomeRecord.OCCURRENCE_IND_DAILY:
                income.occurrence = Income.OccurrenceEnum.Daily;
                break;
            case IncomeRecord.OCCURRENCE_IND_WEEKLY:
                income.occurrence = Income.OccurrenceEnum.Weekly;
                break;
            case IncomeRecord.OCCURRENCE_IND_BIWEEKLY:
                income.occurrence = Income.OccurrenceEnum.BiWeekly;
                break;
            case IncomeRecord.OCCURRENCE_IND_MONTHLY:
                income.occurrence = Income.OccurrenceEnum.Monthly;
                break;
            case IncomeRecord.OCCURRENCE_IND_BIMONTHLY:
                income.occurrence = Income.OccurrenceEnum.BiMonthly;
                break;
            case IncomeRecord.OCCURRENCE_IND_SPECIFIC:
                income.occurrence = Income.OccurrenceEnum.Specific;
                break;
            default:
                throw new RuntimeException("Unable to identify occurrence type: " + record.occurrenceInd);
        }

        return income;
    }
}

package domain;

import com.gwiz.androidutils.db.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import infrastructure.dao.CategoryDao;
import infrastructure.data.CategoryDto;
import infrastructure.record.TransactionDataRecord;
import infrastructure.record.TransactionRecord;
import infrastructure.record.TransactionTransferRecord;

/**
 * Created by Paul on 3/8/2015.
 */
public class TransactionRepo extends Repository<Transaction, String> {

    private CategoryDao categoryDao;

    public TransactionRepo(Database db) {
        super(db);
    }

    @Override
    public Transaction find(String guid) {
        TransactionRecord record = new TransactionRecord();
        record.record.transGuid = guid;

        record.select(this.db, 1);

        Transaction transaction = this.extractFromDao(record);
        transaction.transactionRepo = this;
        return transaction;
    }

    @Override
    public void save(Transaction transaction) {

        TransactionRecord record = new TransactionRecord();

        // TODO need DAO to update or insert this
        // Check if the transaction exists
        record.record.transGuid = transaction.getGuid();
        record.select(this.db, 1);
        boolean doInsert = false;

        // If transaction doesn't exist, we will need to insert; otherwise
        // let's just update
        if(!record.getGoodState()) {
            doInsert = true;
        }

        this.db.startTransaction();

        record.record.amount = transaction.getAmount();
        record.record.comment = transaction.getComment();
        record.record.date = transaction.getDate();
        record.record.budgetGuid = transaction.getBudgetGuid();

        if(doInsert) {
            record.insert(this.db);
        } else {
            record.update(this.db);
        }

        CategoryDto dto = new CategoryDto();
        dto.id = transaction.getCategoryCode();
        dto.categoryName = transaction.getCategoryName();
        this.categoryDao.saveCategory(dto);

        this.db.finishTransaction();
    }

    @Override
    public void delete(Transaction transaction) {

        if(transaction.getGuid().isEmpty()) {
            throw new RuntimeException("Transaction key is empty for deletion.");
        }

        this.db.startTransaction();

        // Remove child records.
        // Remove Transaction Data
        TransactionDataRecord dataRecord = new TransactionDataRecord();
        dataRecord.record.transactionGuid = transaction.getGuid();
        dataRecord.delete(this.db);

        // Do I want to remove transfers??

        // Remove Category
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.id = transaction.getCategoryCode();
        this.categoryDao.deleteCategory(categoryDto);

        // Remove parent transaction.
        TransactionRecord record = new TransactionRecord();
        record.record.transGuid = transaction.getGuid();
        record.delete(this.db);

        this.db.finishTransaction();

    }

    public List<Transaction> findTransactions(String budgetGuid) {

        TransactionRecord record = new TransactionRecord();
        record.record.budgetGuid = budgetGuid;
        List<Transaction> transactions = new ArrayList<>();

        record.select(this.db, 1);
        if(!record.getGoodState()) {
            return transactions;
        }

        do {
            Transaction transaction = this.find(record.record.transGuid);
            transactions.add(transaction);

        }while(record.readNext());

        return transactions;
    }

    public Transaction createNewTransaction() {
        Transaction transaction = new Transaction();
        transaction.setGuid(UUID.randomUUID().toString());

        return transaction;
    }

    private Transaction extractFromDao(TransactionRecord record) {
        Transaction transaction = new Transaction();

        transaction.setGuid(record.record.transGuid);
        transaction.setBudgetGuid(record.record.budgetGuid);
        transaction.setAmount(record.record.amount);
        transaction.setComment(record.record.comment);
        transaction.setDate(record.record.date);

        return transaction;
    }

    protected long findTransactionDataPicId(String transactionGuid) {

        TransactionDataRecord record = new TransactionDataRecord();
        record.record.transactionGuid = transactionGuid;

        record.select(this.db, 1);
        if(!record.getGoodState()) {
            return -1;
        }

        record.getFirst();
        return record.record.picId;
    }

    protected String findOriginBudget(String transactionGuid) {

        TransactionTransferRecord record = new TransactionTransferRecord();
        record.record.transactionGuid = transactionGuid;

        record.select(this.db, 1);

        if(!record.getGoodState()) {
            return "";
        }

        record.getFirst();
        return record.record.originGuid;

    }
}

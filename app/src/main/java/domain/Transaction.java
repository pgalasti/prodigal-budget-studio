package domain;

import com.gwiz.androidutils.util.DateTime;

import java.io.Serializable;

import infrastructure.data.TransactionTransferDto;

/**
 * Created by Paul on 3/8/2015.
 */
public class Transaction implements Serializable {

    protected TransactionRepo transactionRepo;

    protected String guid;

    protected String budgetGuid;
    protected TransactionTypeEnum type;
    protected float amount;
    protected String comment;
    protected DateTime date;
    protected int categoryCode;
    protected String categoryName;
    protected TransactionTransferDto transferDto;

    protected Transaction(){}
    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getBudgetGuid() {
        return budgetGuid;
    }

    protected void setBudgetGuid(String budgetGuid) {
        this.budgetGuid = budgetGuid;
    }

    public TransactionTypeEnum getType() {
        return type;
    }

    public void setType(TransactionTypeEnum type) {
        this.type = type;
    }

    public float getAmount() {
        if(this.type == TransactionTypeEnum.WITHDRAWAL) {
            return amount*-1;
        } else if(this.type == TransactionTypeEnum.CREDIT) {
            return amount;
        } else if(this.type == TransactionTypeEnum.TRANSFER) {
            if(transferDto.originGuid == this.guid) {
                return amount*-1;
            } else {
                return amount;
            }
        }

        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }

    public int getCategoryCode() {
        return this.categoryCode;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String name) {
        this.categoryName = name;
    }

    public long getPicId() {
        return this.transactionRepo.findTransactionDataPicId(this.guid);
    }

    public void savePicture() {

        // TODO write logic to save transactionData + transactionTransfer maybe with member lists?
        this.transactionRepo.save(this);

    }

    public String getTransfersOriginBudgetId() {
        if(this.type != TransactionTypeEnum.TRANSFER) {
            return "";
        }

        return this.transactionRepo.findOriginBudget(this.guid);
    }
}

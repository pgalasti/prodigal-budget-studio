package domain;

import com.gwiz.androidutils.db.Database;

/**
 * Created by Paul on 3/1/2015.
 */
public abstract class Repository<Domain, Key> {

    protected Database db;

    public Repository(Database db) {
        this.db = db;
    }

    public abstract Domain find(Key key);
    public abstract void save(Domain domain);
    public abstract void delete(Domain domain);
}

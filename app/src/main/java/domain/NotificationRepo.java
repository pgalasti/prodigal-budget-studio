package domain;

import com.gwiz.androidutils.db.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import infrastructure.record.NotificationRecord;

/**
 * Created by Paul on 3/1/2015.
 */
public class NotificationRepo extends Repository<Notification, String> {

    public NotificationRepo(Database db) {
        super(db);
    }

    // TODO Create Notification DAO to abstract db logic
    @Override
    public Notification find(String guid) {

        NotificationRecord record = new NotificationRecord();
        record.record.noteificationGuid = guid;

        record.select(this.db, 1);

        return this.extractFromDao(record);
    }

    @Override
    public void save(Notification notification) {

        NotificationRecord record = new NotificationRecord();
        record.record.bReported = notification.getNotified();
        record.record.notificationMessage = notification.getMessage();

        if(notification.guid.isEmpty()) {
        record.record.noteificationGuid = UUID.randomUUID().toString();
            record.insert(this.db);
            return;
        }

        record.record.noteificationGuid = notification.guid;
        record.update(this.db);
    }

    @Override
    public void delete(Notification notification) {

        if(notification.guid.isEmpty())
            throw new RuntimeException("Notification key is empty for deletion.");

        NotificationRecord record = new NotificationRecord();
        record.record.noteificationGuid = notification.guid;
        record.delete(this.db);
    }

    public List<Notification> findUnNotifiedNotifications() {
        List<Notification> notifications = new ArrayList<>();

        NotificationRecord record = new NotificationRecord();

        StringBuilder sbQuery = new StringBuilder();
        sbQuery.append("SELECT * FROM ").append(record.getRecordAttributes().getTableName())
                .append(" WHERE reported=0;");

        record.select(this.db, sbQuery.toString());
        if( record.getGoodState() )
        {
            do
            {
                notifications.add(this.extractFromDao(record));
            }while(record.readNext());
        }

        return notifications;
    }

    private Notification extractFromDao(NotificationRecord record) {
        Notification notification = new Notification();

        notification.guid = record.record.noteificationGuid;
        notification.setMessage(record.record.notificationMessage);
        notification.isNotified = record.record.bReported;

        notification.repo = this;

        return notification;
    }
}

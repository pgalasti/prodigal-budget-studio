package domain;

import com.gwiz.androidutils.db.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import infrastructure.dao.BudgetDao;
import infrastructure.data.BudgetDto;

/**
 * Created by Paul on 3/2/2015.
 */
public class BudgetRepo extends Repository<Budget, String> {


    private BudgetDao budgetDao;
    private TransactionRepo transactionRepo;

    public BudgetRepo(Database db) {

        super(db);
        this.budgetDao = new BudgetDao(db);
        this.transactionRepo = new TransactionRepo(db);
    }

    @Override
    public Budget find(String guid) {

        BudgetDto dto = this.budgetDao.findBudget(guid);
        Budget budget = this.buildBudget(dto);

        budget.transactionRepo = new TransactionRepo(this.db);

        return budget;
    }

    @Override
    public void save(Budget budget) {

        this.db.startTransaction();

        this.budgetDao.saveBudget(this.buildDto(budget));

        for(Transaction transaction : budget.getTransactions())
            this.transactionRepo.save(transaction);


        this.db.finishTransaction();
    }

    @Override
    public void delete(Budget budget) {

        if(budget.guid.isEmpty()) {
            throw new RuntimeException("Budget key is empty for deletion.");
        }

        this.db.startTransaction();

        // Remove child tables
        List<Transaction> transactions = budget.getTransactions();
        for(Transaction transaction : transactions) {
            budget.transactionRepo.delete(transaction);
        }

        // Remove budget
        this.budgetDao.deleteBudget(budget.getId());

        this.db.finishTransaction();
    }

    public boolean doesNameExist(String name) {
        return budgetDao.findBudgetName(name);
    }

    public Budget createNewBudget() {

        Budget budget = new Budget();

        budget.guid = UUID.randomUUID().toString();
        budget.transactionRepo = new TransactionRepo(this.db);

        return budget;
    }

    public List<Budget> getAllBudgets() {

        List<Budget> budgets = new ArrayList<>();
        List<BudgetDto> dtos = this.budgetDao.getAllBudgets();

        for(BudgetDto dto : dtos) {
            Budget budget = this.buildBudget(dto);
            budget.transactionRepo = new TransactionRepo(this.db);

            budgets.add(budget);
        }

        return budgets;
    }

    private Budget buildBudget(BudgetDto dto) {
        Budget budget = new Budget();

        budget.guid = dto.budgetGuid;
        budget.setName(dto.budgetName);
        budget.setLimit(dto.limit);
        budget.setCurrent(dto.current);

        return budget;
    }

    private BudgetDto buildDto(Budget budget) {
        BudgetDto dto = new BudgetDto();
        dto.budgetGuid = budget.getId();
        dto.budgetName = budget.getName();
        dto.current = budget.getCurrent();
        dto.limit = budget.getLimit();

        return dto;
    }
}

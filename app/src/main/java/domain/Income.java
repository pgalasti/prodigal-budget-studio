package domain;

import com.gwiz.androidutils.util.DateTime;

/**
 * Created by pgalasti on 2/1/16.
 */
public class Income {

    protected String guid;

    protected enum OccurrenceEnum {
        Daily,
        Weekly,
        BiWeekly,
        Monthly,
        BiMonthly,
        Specific,
    }

    protected String name;
    protected float amount;
    protected OccurrenceEnum occurrence;
    protected boolean reoccurrence;
    protected DateTime date;

    protected Income() {
        this.guid = "";

        this.name = "";
        this.amount = 0.0f;
        this.occurrence = OccurrenceEnum.Daily;
        this.reoccurrence = false;
        this.date = new DateTime();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        if(name.isEmpty()) {
            throw new RuntimeException("Income name cannot be blank.");
        }

        this.name = name;
    }

    public float getAmount() {
        return this.amount;
    }

    public void setAmount(float amount) {
        if(amount <= 0.0) {
            throw new RuntimeException("Amount cannot be 0 or smaller.");
        }
        this.amount = amount;
    }

    public boolean isOccurrenceDaily() {
        return this.occurrence == OccurrenceEnum.Daily;
    }

    public boolean isOccurrenceWeekly() {
        return this.occurrence == OccurrenceEnum.Weekly;
    }

    public boolean isOccurrenceBiWeekly() {
        return this.occurrence == OccurrenceEnum.BiWeekly;
    }

    public boolean isOccurrenceMonthly() {
        return this.occurrence == OccurrenceEnum.Monthly;
    }

    public boolean isOccurrenceBiMonthly() {
        return this.occurrence == OccurrenceEnum.BiMonthly;
    }

    public boolean isOccurrenceSpecificDate() {
        return this.occurrence == OccurrenceEnum.Specific;
    }

    public void setOccurrenceDaily() {
        this.occurrence = OccurrenceEnum.Daily;
    }

    public void setOccurrenceWeekly() {
        this.occurrence = OccurrenceEnum.Weekly;
    }

    public void setOccurrenceBiWeekly() {
        this.occurrence = OccurrenceEnum.BiWeekly;
    }

    public void setOccurrenceMonthly() {
        this.occurrence = OccurrenceEnum.Monthly;
    }

    public void setOccurrenceBiMonthly() {
        this.occurrence = OccurrenceEnum.BiMonthly;
    }

    public void setOccurrenceSpecificDate() {
        this.occurrence = OccurrenceEnum.Specific;
    }

    public boolean isReoccurring() {
        return this.reoccurrence;
    }

    public void setReoccurring(boolean reoccurrence) {
        this.reoccurrence = reoccurrence;
    }

    public DateTime getDate() {
        return new DateTime(this.date.getTimeInMilliseconds());
    }

    public void setDate(DateTime date) {
        this.date = new DateTime(date.getTimeInMilliseconds());
        this.date.setTimeToStartOfDay();
    }

    public String getOccurrenceName() {

        String occurrenceName = "";
        switch(this.occurrence)
        {
            case Daily:
                occurrenceName = "Daily";
                break;

            case Weekly:
                occurrenceName = "Weekly";
                break;

            case BiWeekly:
                occurrenceName = "Bi-Weekly";
                break;

            case Monthly:
                occurrenceName = "Monthly";
                break;

            case BiMonthly:
                occurrenceName = "Bi-Monthly";
                break;

            case Specific:
                occurrenceName = "Specific Date";
                break;
        }

        return occurrenceName;
    }




}

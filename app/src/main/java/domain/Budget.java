package domain;

import com.gwiz.androidutils.util.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Paul on 3/2/2015.
 */
public class Budget {

    protected String guid;
    protected String name;
    protected float limit;
    protected float current;

    protected TransactionRepo transactionRepo;

    protected Budget() {}

    public String getId() {
        return this.guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLimit() {
        return limit;
    }

    public void setLimit(float limit) {
        if (limit < 0.0f) {
            throw new RuntimeException("Unable to set a budget limit below 0.");
        }

        this.limit = limit;
    }

    public float getCurrent() {
        return current;
    }

    public void setCurrent(float current) {
        if(current < 0.0f) {
            throw new RuntimeException("Unable to set a budget current amount below 0.");
        }
        this.current = current;
    }

    public final float getCurrentToLimitRatio() {

        float ratio = 0.0f;
        if(limit > 0.0)
            ratio = current/limit;

        return ratio;
    }

    public Transaction createTransaction() {
        Transaction transaction = this.transactionRepo.createNewTransaction();
        transaction.setBudgetGuid(this.guid);

        return transaction;
    }

    public List<Transaction> getTransactions(DateTime start, DateTime end) {
        List<Transaction> allTransactions = transactionRepo.findTransactions(this.guid);

        // Lossy and need to revise this. TODO

        List<Transaction> filteredList = new ArrayList<>();
        for(Transaction transaction : allTransactions) {
            // If the date is before start or after the end, skip.
            if(DateTime.isDateBefore(transaction.getDate(), start)) {
                continue;
            }
            else if(!DateTime.isDateBefore(transaction.getDate(), end)) {
                continue;
            }
            filteredList.add(transaction);
        }

        return filteredList;

    }

    public List<Transaction> getTransactions() {
        return transactionRepo.findTransactions(this.guid);
    }

    private enum TransferTypeEnum {
        WITHDRAWAL,
        CREDIT
    }

    public List<Transaction> getCreditTransfers() {
        return this.getTransfers(TransferTypeEnum.CREDIT);
    }

    public List<Transaction> getWithdrawalTransfers() {
        return this.getTransfers(TransferTypeEnum.WITHDRAWAL);
    }

    private List<Transaction> getTransfers(TransferTypeEnum type) {

        List<Transaction> budgetTransactions = this.getTransactions();

        List<Transaction> filteredList = new ArrayList<>();
        for(Transaction transaction : budgetTransactions) {


            if(transaction.getTransfersOriginBudgetId() != this.guid) {
                if(type == TransferTypeEnum.CREDIT) {
                    filteredList.add(transaction);
                }
            } else {
                if(type == TransferTypeEnum.WITHDRAWAL) {
                    filteredList.add(transaction);
                }
            }
        }
        return filteredList;
    }

}

package domain;

import java.io.Serializable;

/**
 * Created by Paul on 3/1/2015.
 */
public class Notification implements Serializable{

    protected NotificationRepo repo;

    protected String guid;
    protected String message;
    protected boolean isNotified;

    protected Notification() {}

    public void saveAsNotified() {
        this.setNotified(true);
        this.repo.save(this);
    }

    public void saveAsUnNotified() {
        this.setNotified(false);
        this.repo.save(this);
    }



    public void delete() {
        this.repo.delete(this);
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setNotified(boolean isNotified) {
        this.isNotified = isNotified;
    }

    public boolean getNotified() {
        return this.isNotified;
    }

}

package domain;

/**
 * Created by Paul on 3/8/2015.
 */
public enum TransactionTypeEnum {

    CREDIT,
    WITHDRAWAL,
    TRANSFER;

    public static char getCharacterType(TransactionTypeEnum enumeration) {
        char type = ' ';
        switch(enumeration) {
            case CREDIT:
                type = 'C';
                break;
            case WITHDRAWAL:
                type = 'W';
                break;
            case TRANSFER:
                type = 'T';
                break;
        }
        return type;
    }
}

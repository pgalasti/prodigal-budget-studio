package com.gwiz.prodigalbudget.prodigalbudget.activity;

import applogic.InitializationManager.LastActivityEnum;

public interface IActivityType
{
	public LastActivityEnum activityIdentity();
}

package com.gwiz.prodigalbudget.prodigalbudget.activity;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;

import android.util.Log;
import applogic.InitializationManager;
import applogic.InitializationSingleton;
import com.gwiz.prodigalbudget.R;

public abstract class ProdigalActivity extends FragmentActivity implements IActivityType {
	
	protected InitializationManager initManager = null;
	protected ActionBar actionBar;

	private static final String TAG = "ProdigalActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);

		Log.d(TAG, "Starting Activity: " + this.getLocalClassName());

        this.initManager = InitializationSingleton.getInstance(getApplicationContext());
        this.actionBar = getActionBar();

		this.actionBar.setDisplayShowCustomEnabled(true);

		// Set Prodigal Blue color to action bar
		final int color = getResources().getColor(R.color.prodigal_blue);
		this.actionBar.setBackgroundDrawable(new ColorDrawable(color));

		this.initManager.setLastActivity(activityIdentity());
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		this.initManager.setLastActivity(activityIdentity());
	}

	@Override
	protected void onStop()
	{
		super.onStop();

	}
}

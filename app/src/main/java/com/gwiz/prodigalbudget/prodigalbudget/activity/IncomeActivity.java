package com.gwiz.prodigalbudget.prodigalbudget.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import applogic.InitializationManager;
import com.gwiz.androidutils.util.DateTime;
import com.gwiz.prodigalbudget.R;
import domain.Income;
import domain.IncomeRepo;
import infrastructure.db.ProdigalDatabaseSingleton;

/**
 * Created by pgalasti on 1/28/16.
 */
public class IncomeActivity extends ProdigalActivity {

    protected final static int CALENDAR_POPUP_REQUEST_CODE = 0x0001;
    public final static String ActivityTypeKey = "launch_type";
    public final static String ActivityMessageAddIncome = "add_income";
    public final static String ActivityMessageEditIncome = "edit_income";

    protected LinearLayout parentLayout;
    protected Button dateButton;
    protected Button saveButton;
    protected EditText incomeNameText;
    protected EditText incomeAmountText;
    protected RadioGroup occurrenceRadioGroup;
    protected RadioButton dailyOccurrenceRadioButton;
    protected RadioButton weeklyOccurrenceRadioButton;
    protected RadioButton biWeeklyOccurrenceRadioButton;
    protected RadioButton monthlyOccurrenceRadioButton;
    protected RadioButton biMonthlyOccurrenceRadioButton;
    protected RadioButton specificDateOccurrenceRadioButton;
    protected CheckBox reoccurringCheckbox;


    protected DateTime dateToUse;
    protected boolean isAddIncome = false;
    protected boolean isEditIncome = false;

    protected IncomeRepo incomeRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set layout
        setContentView(R.layout.activity_income_screen);
        this.actionBar.show();
        this.actionBar.setDisplayHomeAsUpEnabled(true);

        // Grab extras from launch; setup based on values
        Bundle activityExtras = getIntent().getExtras();
        String activityType = activityExtras.getString(ActivityTypeKey);
        this.isAddIncome = (activityType.compareToIgnoreCase(ActivityMessageAddIncome) == 0);
        this.isEditIncome = (activityType.compareToIgnoreCase(ActivityMessageEditIncome) == 0);

        if(this.isAddIncome) {
            setTitle(getResources().getString(R.string.save_income));
        } else if(this.isEditIncome) {
            setTitle(getResources().getString(R.string.edit_income));
        }

        this.bindControls();

        this.setDateToUse(new DateTime().getTimeInMilliseconds());
        this.incomeRepo = new IncomeRepo(ProdigalDatabaseSingleton.getInstance(getApplicationContext()));
    }

    @Override
    protected void onActivityResult(int aRequestCode, int aResultCode, Intent aData) {
        if(aResultCode == Activity.RESULT_CANCELED)
            return;

        switch (aRequestCode) {
            case CALENDAR_POPUP_REQUEST_CODE:
                final long timeInMilliseconds = aData.getLongExtra("date_result", System.currentTimeMillis());
                this.setDateToUse(timeInMilliseconds);
            break;
            default:
                break;
        }

    }

    protected void setDateToUse(long dateInMilliseconds) {
        this.dateToUse = new DateTime(dateInMilliseconds);
        this.dateButton.setText(this.dateToUse.dateTimeToString(DateTime.MMDDCCYY));
    }

    @Override
    public InitializationManager.LastActivityEnum activityIdentity() {
        return InitializationManager.LastActivityEnum.IncomeActivity;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
        }

        return true;
    }

    private void bindControls() {

        // Bind controls; set action listeners
        this.parentLayout = (LinearLayout)findViewById(R.id.income_parent_layout_ll);
        this.dateButton = (Button)findViewById(R.id.income_date_button);
        this.dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent calendarPopupIntent = new Intent(getApplicationContext(), CalendarPopupActivity.class);
                calendarPopupIntent.putExtra("date_data", dateToUse.getTimeInMilliseconds());
                startActivityForResult(calendarPopupIntent, CALENDAR_POPUP_REQUEST_CODE);
            }
        });

        this.saveButton = (Button)findViewById(R.id.income_save_button);
        this.saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!validateFields()) {
                    return;
                }

                Income income = incomeRepo.create();
                income.setName(incomeNameText.getText().toString());
                income.setAmount(Float.valueOf(incomeAmountText.getText().toString()));
                income.setDate(dateToUse);
                income.setReoccurring(reoccurringCheckbox.isChecked());
                switch(occurrenceRadioGroup.getCheckedRadioButtonId())
                {
                    case R.id.income_daily_radiobutton:
                        income.setOccurrenceDaily();
                        break;
                    case R.id.income_weekly_radiobutton:
                        income.setOccurrenceWeekly();
                        break;
                    case R.id.income_biweekly_radiobutton:
                        income.setOccurrenceBiWeekly();
                        break;
                    case R.id.income_monthly_radiobutton:
                        income.setOccurrenceMonthly();
                        break;
                    case R.id.income_bimonthly_radiobutton:
                        income.setOccurrenceBiMonthly();
                        break;
                    case R.id.income_specificdate_radiobutton:
                        income.setOccurrenceSpecificDate();
                        break;
                    default:
                        throw new RuntimeException("Unable to determine the occurrence type while saving income.");
                }

                incomeRepo.save(income);
                Toast.makeText(getBaseContext(), R.string.income_saved, Toast.LENGTH_LONG);
                finish();

            }
        });

        this.incomeNameText = (EditText)findViewById(R.id.income_label_edittext);
        this.incomeNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        this.incomeAmountText = (EditText)findViewById(R.id.income_amount_edittext);
        this.incomeAmountText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        this.occurrenceRadioGroup = (RadioGroup)findViewById(R.id.occurrence_radiogroup);
        this.dailyOccurrenceRadioButton = (RadioButton)findViewById(R.id.income_daily_radiobutton);
        this.weeklyOccurrenceRadioButton = (RadioButton)findViewById(R.id.income_weekly_radiobutton);
        this.biWeeklyOccurrenceRadioButton= (RadioButton)findViewById(R.id.income_biweekly_radiobutton);
        this.monthlyOccurrenceRadioButton = (RadioButton)findViewById(R.id.income_monthly_radiobutton);
        this.biMonthlyOccurrenceRadioButton = (RadioButton)findViewById(R.id.income_bimonthly_radiobutton);
        this.specificDateOccurrenceRadioButton = (RadioButton)findViewById(R.id.income_specificdate_radiobutton);
        this.reoccurringCheckbox = (CheckBox)findViewById(R.id.income_reoccurring_checkbox);
    }

    protected boolean validateFields() {

        if(this.incomeNameText.getText().toString().isEmpty()) {
            return false;
        }

        if(this.incomeAmountText.getText().toString().isEmpty()) {
            return false;
        }

        if(this.occurrenceRadioGroup.getCheckedRadioButtonId() == -1) {
            return false;
        }

        return true;
    }
}

package com.gwiz.prodigalbudget.prodigalbudget.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gwiz.prodigalbudget.R;

import applogic.InitializationManager.LastActivityEnum;
import domain.Budget;
import domain.BudgetRepo;
import infrastructure.db.ProdigalDatabaseSingleton;

public class AddBudgetActivity extends ProdigalActivity {

	public final static String ActivityTypeKey = "launch_type";
    public final static String BudgetIdKey = "budget_id";
	public final static String ActivityMessageAddBudget = "add_budget";
	public final static String ActivityMessageEditBudget = "edit_budget";
	
	Button addEditButton;
	EditText budgetNameEditText;
	EditText budgetLimitEditText;

	boolean isAddBudget = false;
	boolean isEditBudget = false;

    BudgetRepo budgetRepo;
    String editGuid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_budget_layout);
		
		budgetNameEditText = (EditText)findViewById(R.id.add_budget_name_edittext);
		budgetLimitEditText = (EditText)findViewById(R.id.add_budget_limit_edittext);
		addEditButton = (Button)findViewById(R.id.add_budget_save_button);

        budgetRepo = new BudgetRepo(ProdigalDatabaseSingleton.getInstance(getApplicationContext()));

        this.actionBar.show();
        this.actionBar.setDisplayHomeAsUpEnabled(true);

		addEditButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if( isAddBudget ) {
                    addBudget();
				} else {
                    editBudget();
                }
			}
		});

		Bundle activityExtras = getIntent().getExtras();
		String activityType = activityExtras.getString(ActivityTypeKey);
		isAddBudget = (activityType.compareToIgnoreCase(ActivityMessageAddBudget) == 0);
		isEditBudget = (activityType.compareToIgnoreCase(ActivityMessageEditBudget) == 0);
		
        if(isAddBudget) {
            setTitle("Add Budget");
        } else if(isEditBudget) {
            setTitle("Edit Budget");
        }

	}

    @Override
    public void onResume() {
        super.onResume();;
        if(!isEditBudget) {
            return;
        }

        // Get our budget.
        Bundle activityExtras = getIntent().getExtras();
        final String budgetToEditGuid = activityExtras.getString(BudgetIdKey);
        Budget budget = this.budgetRepo.find(budgetToEditGuid);
        this.populateUIWithBudget(budget);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
        }

        return true;
    }

    private boolean validateEditBoxes() {
        if( budgetNameEditText.getText().toString().isEmpty() || budgetLimitEditText.getText().toString().isEmpty()	)
            return false;

        final float value = Float.parseFloat(budgetLimitEditText.getText().toString());
        if( value <= 0.0f ) {
            return false;
        }
        return true;
    }

    private Budget buildBudgetFromUI() {

        // refactor this, it's ugly
        Budget budget = budgetRepo.createNewBudget();
        budget.setName(budgetNameEditText.getText().toString());
        budget.setLimit(Float.parseFloat(budgetLimitEditText.getText().toString()));

        return budget;
    }

	private void addBudget() {
        this.validateEditBoxes();

        Budget budget = this.buildBudgetFromUI();

        if(budgetRepo.doesNameExist(budget.getName())) {
            Toast.makeText(getApplicationContext(),
                    budget.getName() + " already exists as a budget name." ,
                    Toast.LENGTH_LONG).show();
            return;
        }

        this.budgetRepo.save(budget);

		Toast.makeText(getApplicationContext(), budget.getName() + " added." , Toast.LENGTH_LONG).show();
		finish();		
	}

    private void editBudget() {
        this.validateEditBoxes();

        Bundle activityExtras = getIntent().getExtras();
        final String budgetToEditGuid = activityExtras.getString(BudgetIdKey);
        Budget budget = this.budgetRepo.find(budgetToEditGuid);
        budget.setName(budgetNameEditText.getText().toString());
        budget.setLimit(Float.parseFloat(budgetLimitEditText.getText().toString()));

        this.budgetRepo.save(budget);

        Toast.makeText(getApplicationContext(), budget.getName() + " edited." , Toast.LENGTH_LONG).show();
        finish();
    }

    private void populateUIWithBudget(Budget budget) {
        this.budgetNameEditText.setText(budget.getName());
        this.budgetLimitEditText.setText(String.valueOf(budget.getLimit()));
    }

	@Override
	public LastActivityEnum activityIdentity() {
		return LastActivityEnum.AddBudgetActivity;
	}
}

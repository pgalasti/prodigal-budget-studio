package com.gwiz.prodigalbudget.prodigalbudget.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import applogic.InitializationManager;
import com.gwiz.androidutils.util.DateTime;
import com.gwiz.prodigalbudget.R;

/**
 * Created by pgalasti on 1/29/16.
 */
public class CalendarPopupActivity extends ProdigalActivity {


    protected CalendarView calendarView;
    protected Button saveButton;
    protected Button cancelButton;


    protected DateTime selectedDateTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_calendar_popup_layout);



        // Initialize the date as today
        final long initDateInMilliseconds = getIntent().getExtras().getLong("date_data");
        this.selectedDateTime = new DateTime(initDateInMilliseconds);

        // Setup the screen metrics and layout
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.getWindow().setLayout(
                (int)(displayMetrics.widthPixels*.8),
                (int)(displayMetrics.heightPixels*.8));
        this.getWindow().setBackgroundDrawableResource(R.drawable.popup_shape_border_background);

        this.actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_shape_background));

        // Control events
        calendarView = (CalendarView)findViewById(R.id.calendar_popup);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                selectedDateTime = new DateTime(month+1, dayOfMonth, year);
                setTitle(selectedDateTime.dateTimeToString(DateTime.MMDDCCYY));
            }
        });

        this.saveButton = (Button)findViewById(R.id.calendar_popup_save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("date_result", selectedDateTime.getTimeInMilliseconds());
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });

        this.cancelButton = (Button)findViewById(R.id.calendar_popup_cancel_button);
        this.cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED, null);
                finish();
            }
        });

        this.calendarView.setDate(initDateInMilliseconds);
        this.setTitle(selectedDateTime.dateTimeToString(DateTime.MMDDCCYY));
    }

    @Override
    public InitializationManager.LastActivityEnum activityIdentity() {
        // May change this later if we need another calendar popup..
        return InitializationManager.LastActivityEnum.IncomeActivity;
    }

    protected void setTitle(String title) {
        // TODO An ugly solution for now; use the appropriate style later
        this.actionBar.setTitle(Html.fromHtml("<font color=\"black\"> Date Selected: " + title + "</font>"));
    }


}

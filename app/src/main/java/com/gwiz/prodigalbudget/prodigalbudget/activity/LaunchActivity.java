package com.gwiz.prodigalbudget.prodigalbudget.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.gwiz.androidutils.db.IQueryWritable;
import com.gwiz.prodigalbudget.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

import applogic.InitializationManager.LastActivityEnum;
import domain.Budget;
import domain.BudgetRepo;
import domain.NotificationRepo;
import infrastructure.db.DBRecordException;
import infrastructure.db.ProdigalBudgetDatabase;
import infrastructure.db.ProdigalDatabaseSingleton;
import infrastructure.record.NotificationRecord;

public class LaunchActivity extends ProdigalActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if( initManager.checkForFirstTimeRun() )
		{
			ProdigalBudgetDatabase db = ProdigalDatabaseSingleton.getInstance(getApplicationContext());
			ArrayList<IQueryWritable> recordList = getInitialDatabaseRecords();
			
			try
			{
				for( Iterator<IQueryWritable> recordIter = recordList.iterator(); recordIter.hasNext(); )
				{
					IQueryWritable nextRecord = recordIter.next();
					nextRecord.insert(db);
				}
			}
			catch(DBRecordException.DatabaseWritableException e)
			{
				//TODO handle error for failing to put tutorial records.
			}
			
			try {
				initManager.setFirstTimeRun();
			} catch (IOException e) {
				PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear();
				Toast.makeText(getBaseContext(), "The app was unable to apply first time run optimizations!", Toast.LENGTH_LONG).show();
			}
		}
		
		// TODO
		if( initManager.getLastActivity() == LastActivityEnum.BudgetActivity 
				|| initManager.getLastActivity() == LastActivityEnum.StatisticsActivity ) // For now
		{
			Intent newIntent = new Intent(getApplicationContext(), MainScreenActivity.class);
			startActivity(newIntent);
			finish();
		}
	}
	
	private ArrayList<IQueryWritable> getInitialDatabaseRecords()
	{
		ArrayList<IQueryWritable> databaseRecords = new ArrayList<IQueryWritable>();
		
		BudgetRepo budgetRepo = new BudgetRepo(ProdigalDatabaseSingleton.getInstance(getApplicationContext()));
        Budget budget = budgetRepo.createNewBudget();
        budget.setName(getBaseContext().getString(R.string.sample_mortgage_label));
        budget.setCurrent(0.00f);
        budget.setLimit(1001.0f);
		budgetRepo.save(budget);


        budget = budgetRepo.createNewBudget();
        budget.setName(getBaseContext().getString(R.string.sample_grocery_label));
        budget.setCurrent(0.00f);
        budget.setLimit(150);
		budgetRepo.save(budget);
		
        budget = budgetRepo.createNewBudget();
        budget.setName(getBaseContext().getString(R.string.sample_eatout_label));
        budget.setCurrent(0.00f);
        budget.setLimit(50);
		budgetRepo.save(budget);

        budget = budgetRepo.createNewBudget();
        budget.setName(getBaseContext().getString(R.string.sample_gas_label));
        budget.setCurrent(0.00f);
        budget.setLimit(120);
		budgetRepo.save(budget);
		
		// Add rest...

        // Do later
        NotificationRepo notificationRepo = new NotificationRepo(ProdigalDatabaseSingleton.getInstance(getApplicationContext()));

		NotificationRecord notificationRecord;
		notificationRecord = new NotificationRecord();
		notificationRecord.record.noteificationGuid = UUID.randomUUID().toString();
		notificationRecord.record.bReported = false;
		notificationRecord.record.notificationMessage = getBaseContext().getString(R.string.str_welcome);
		databaseRecords.add(notificationRecord);
		
		notificationRecord = new NotificationRecord();
		notificationRecord.record.noteificationGuid = UUID.randomUUID().toString();
		notificationRecord.record.bReported = false;
		notificationRecord.record.notificationMessage = getBaseContext().getString(R.string.str_notification_tutorial);
		databaseRecords.add(notificationRecord);
		
		// Add rest...
		return databaseRecords;
	}

	@Override
	public LastActivityEnum activityIdentity() {
		return LastActivityEnum.LaunchActivity;
	}

}

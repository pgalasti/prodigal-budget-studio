package com.gwiz.prodigalbudget.prodigalbudget.activity;

import android.app.ActionBar;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import applogic.InitializationManager;
import com.gwiz.androidutils.util.DateTime;
import com.gwiz.prodigalbudget.R;
import domain.Income;
import domain.IncomeRepo;
import infrastructure.db.ProdigalDatabaseSingleton;
import ui.IncomeItem;

import java.util.List;

public class IncomeListActivity extends ProdigalActivity {

    /** Controls **/
    private LinearLayout listLayout;
    private TextView incomeTotalAmountTextView;
    private ProgressBar incomeProgress;
    /** Acitivty Logic **/
    private IncomeRepo incomeRepo;
    private DateTime date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set layout
        setContentView(R.layout.activity_income_list_layout);
        this.actionBar.show();
        this.actionBar.setDisplayHomeAsUpEnabled(true);

        this.listLayout = (LinearLayout)findViewById(R.id.income_list_main);
        this.incomeTotalAmountTextView = (TextView)findViewById(R.id.income_amount_of);
        this.incomeProgress = (ProgressBar)findViewById(R.id.income_total_progress);

        this.incomeRepo = new IncomeRepo(ProdigalDatabaseSingleton.getInstance(getApplicationContext()));
        this.date = new DateTime();
    }

    @Override
    public void onResume() {
        super.onResume();

        this.listLayout.removeAllViews();
        this.drawIncomeList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_budgetlist_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void drawIncomeList() {

        List<Income> incomeList = this.incomeRepo.getAllIncomes();
        float totalIncomeAmount = 0.0f;
        float totalIncomeCollected = 0.0f;

        int delay = 0;
        for(Income income : incomeList) {

            totalIncomeAmount += income.getAmount();
            if(income.getDate().getTimeInMilliseconds() <= this.date.getTimeInMilliseconds()) {
                totalIncomeCollected += income.getAmount();
            }

            IncomeItem incomeItem = new IncomeItem(getBaseContext(), income, this.date);
            this.listLayout.addView(incomeItem);

            // TODO this is lazy
            LinearLayout divider = new LinearLayout(getBaseContext());
            divider.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
            divider.setBackgroundColor(getResources().getColor(R.color.prodigal_blue));
            divider.setMinimumHeight(1);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            divider.setMinimumWidth((int)(displayMetrics.widthPixels*.8));
            this.listLayout.addView(divider);

            incomeItem.startIncomeItemSlideAnimation(delay);

            incomeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            incomeItem.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            });
        }

        // TODO currency locale
        this.incomeTotalAmountTextView.setText("$ " + String.valueOf(totalIncomeCollected)
                + " of " + String.valueOf(totalIncomeAmount)
                + " collected this month.");

        Log.d("progress", String.valueOf((int)(((totalIncomeCollected/totalIncomeAmount))*100)));
        incomeProgress.setProgress((int)(((totalIncomeCollected/totalIncomeAmount))*100));
    }

    @Override
    public InitializationManager.LastActivityEnum activityIdentity() {
        return InitializationManager.LastActivityEnum.IncomeListActivity;
    }
}
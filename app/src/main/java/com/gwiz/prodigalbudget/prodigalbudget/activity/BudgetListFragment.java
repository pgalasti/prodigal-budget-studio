package com.gwiz.prodigalbudget.prodigalbudget.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gwiz.prodigalbudget.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import domain.Budget;
import domain.BudgetRepo;
import domain.Income;
import domain.IncomeRepo;
import infrastructure.db.ProdigalDatabaseSingleton;
import ui.BudgetItem;

public class BudgetListFragment extends Fragment {

	protected LinearLayout rootLayout;
    protected ProgressBar totalProgress;
    protected TextView totalAmount;
    protected TextView availableText;
    protected LinearLayout incomeIconLayout;
    protected LinearLayout incomeRemainingLayout;
    private Menu actionBarMenu;

    protected ArrayList<BudgetItem> budgetItems = new ArrayList<BudgetItem>();
    protected String budgetEditGuid;

    private BudgetRepo budgetRepo;
    private IncomeRepo incomeRepo;

	public BudgetListFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

        this.budgetRepo = new BudgetRepo(ProdigalDatabaseSingleton.getInstance(getActivity()));
        this.incomeRepo = new IncomeRepo(ProdigalDatabaseSingleton.getInstance(getActivity()));
	}

	@Override
	public void onStop() {
		super.onStop();
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_budgetlist, container, false);
		rootLayout = (LinearLayout) view.findViewById(R.id.budgetlist_main);

		totalProgress = (ProgressBar)view.findViewById(R.id.budget_total_progress);
		totalAmount = (TextView)view.findViewById(R.id.budget_amount_of);
		availableText = (TextView)view.findViewById(R.id.budget_income_available);
        incomeIconLayout = (LinearLayout)view.findViewById(R.id.income_button_layout);
        incomeIconLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent incomeActivityIntent = new Intent(getActivity().getApplicationContext(), IncomeActivity.class);
                incomeActivityIntent.putExtra(IncomeActivity.ActivityTypeKey, IncomeActivity.ActivityMessageAddIncome);
                startActivity(incomeActivityIntent);
            }
        });

        this.incomeRemainingLayout = (LinearLayout)view.findViewById(R.id.total_monthly);
        this.incomeRemainingLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent incomeListActivityIntent = new Intent(getActivity().getApplicationContext(), IncomeListActivity.class);
//                incomeActivityIntent.putExtra(IncomeActivity.ActivityTypeKey, IncomeActivity.ActivityMessageAddIncome);
                startActivity(incomeListActivityIntent);
            }
        });


        registerForContextMenu(rootLayout);
		return view;
	}

	@Override 
	public void onResume() {
		super.onResume();

        budgetItems.clear();
        rootLayout.removeAllViews();

        List<Income> incomeList = this.incomeRepo.getAllIncomes();
        double totalIncome = 0.0f;
        for(Income income : incomeList) {
            totalIncome += income.getAmount();
        }
        availableText.setText("$1500.00 available of " + Double.toString(totalIncome) + " ($200.00 not in budget)");

        calculateAndDrawBudgetList();
	}

    private void calculateAndDrawBudgetList() {

        List<Budget> budgets = this.budgetRepo.getAllBudgets();
        float totalSpent = 0.0f;
        float total = 0.0f;

        int delay = 0;
        for(Budget budget : budgets)
        {
            // Get totals
            totalSpent += budget.getCurrent();
            total += budget.getLimit();

            // Get animation going
            BudgetItem nextBudgetItem = new BudgetItem(rootLayout.getContext(), budget);
            rootLayout.addView(nextBudgetItem);
            nextBudgetItem.startBudgetItemSlideAnimation(delay);

            nextBudgetItem.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    BudgetItem itemSelected = (BudgetItem)v;
                    String key = TransactionActivity.Budget_ID;
                    final String budgetId = itemSelected.getBudget().getId();

                    Intent transactionListIntent = new Intent(getActivity().getApplicationContext(), TransactionActivity.class);
                    transactionListIntent.putExtra(key, budgetId);
                    startActivity(transactionListIntent);
                }
            });

            nextBudgetItem.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    // Pretty horrible hack
                    budgetEditGuid = ((BudgetItem)v).getBudget().getId();
                    getActivity().openContextMenu(v);
                    return true;
                }
            });

            this.budgetItems.add(nextBudgetItem);
            delay += 100;
        }

        if(this.budgetItems.isEmpty()) {
            TextView text = new TextView(getActivity());
            text.setText("No budgets... Some text that says \"Click here to create a budget\"?");
            rootLayout.addView(text);
        }

        this.calculateAndDrawTotalSpent(totalSpent, total);
    }

    private void calculateAndDrawTotalSpent(float totalSpent, float total) {
        NumberFormat formatter = NumberFormat.getCurrencyInstance(); // move this to top
        StringBuilder sb = new StringBuilder(formatter.format(totalSpent));
        sb.append(" of ");
        sb.append(formatter.format(total));

        totalAmount.setText(sb.toString());
        totalAmount.invalidate();
        totalAmount.refreshDrawableState();
        totalProgress.setProgress(80);
    }

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.activity_budgetlist_menu, menu);
		actionBarMenu = menu;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

        this.catchMenuItemSelected(item);
		return super.onOptionsItemSelected(item);
	}

    private void catchMenuItemSelected(MenuItem item) {

        String key = "";
        String message;
        Intent addBudgetIntent;
        switch(item.getItemId())
        {
            case R.id.add_budget_btn:
                key = AddBudgetActivity.ActivityTypeKey;
                message = AddBudgetActivity.ActivityMessageAddBudget;

                addBudgetIntent = new Intent(getActivity().getApplicationContext(), AddBudgetActivity.class);
                addBudgetIntent.putExtra(key, message);
                startActivity(addBudgetIntent);
                break;
            case R.id.edit_budget_context_item:
                key = AddBudgetActivity.ActivityTypeKey;
                message = AddBudgetActivity.ActivityMessageEditBudget;

                String editKey = AddBudgetActivity.BudgetIdKey;

                View v = item.getActionView();

                addBudgetIntent = new Intent(getActivity().getApplicationContext(), AddBudgetActivity.class);
                addBudgetIntent.putExtra(key, message);
                addBudgetIntent.putExtra(editKey, budgetEditGuid);
                startActivity(addBudgetIntent);
                break;
            case R.id.delete_budget_context_item:
                Budget budget = this.budgetRepo.find(budgetEditGuid);
                this.deleteBudget(budget);
                break;
            default:
                break;
        }
    }

    public void deleteBudget(Budget budget) {

        for(BudgetItem item : this.budgetItems) {
                rootLayout.removeView(item);
                rootLayout.invalidate();
                rootLayout.refreshDrawableState();
            }

        this.budgetItems.clear();
        this.budgetRepo.delete(budget);

        calculateAndDrawBudgetList();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        LinearLayout layout = (LinearLayout)v;
        MenuInflater inflater = new MenuInflater(getActivity().getApplicationContext());
        inflater.inflate(R.menu.activity_budgetlist_budgetitem_context_menu, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        this.catchMenuItemSelected(item);
        return super.onContextItemSelected(item);
    }
}

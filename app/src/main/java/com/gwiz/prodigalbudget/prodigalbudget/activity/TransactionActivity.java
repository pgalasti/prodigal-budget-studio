package com.gwiz.prodigalbudget.prodigalbudget.activity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.gwiz.androidutils.ui.stickylistheaders.StickyListHeadersListView;
import com.gwiz.androidutils.util.DateTime;
import com.gwiz.prodigalbudget.R;

import java.util.Arrays;
import java.util.Comparator;

import applogic.InitializationManager.LastActivityEnum;
import domain.Budget;
import domain.BudgetRepo;
import domain.Transaction;
import domain.TransactionTypeEnum;
import infrastructure.db.ProdigalDatabaseSingleton;
import ui.AdapterListener;
import ui.StickyListTransactionAdapter;

public class TransactionActivity extends ProdigalActivity implements AdapterListener {

	public final static String Budget_ID = "budget_serialized";
	private Budget budget;
	private BudgetRepo budgetRepo;

    private MenuItem deleteTransactionsButton;

	private Transaction[] testRecords = new Transaction[25];
    StickyListTransactionAdapter listAdapter;
	DateTime beginDate;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transactions_list);

        budgetRepo = new BudgetRepo(ProdigalDatabaseSingleton.getInstance(getApplicationContext()));
		beginDate = new DateTime();
		beginDate.changeDays(-90);
		
		this.actionBar.show();
        this.actionBar.setDisplayHomeAsUpEnabled(true);

		Bundle activityExtras = getIntent().getExtras();
		String budgetGuid = activityExtras.getString(Budget_ID, "");

        budget = budgetRepo.find(budgetGuid);

		setTitle(budget.getName());

        LoadTestRecords();
        StickyListHeadersListView stickyList = (StickyListHeadersListView)findViewById(R.id.transaction_list);
		this.listAdapter = new StickyListTransactionAdapter(getBaseContext(), testRecords);
        this.listAdapter.addListener(this);
		stickyList.setAdapter(this.listAdapter);
	}
	@Override
	public LastActivityEnum activityIdentity() {
		return LastActivityEnum.TransactionsActivity;
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_transactionlist_menu, menu);

        this.deleteTransactionsButton = (MenuItem)menu.findItem(R.id.delete_transaction_btn);
        // Initially hide it
        this.deleteTransactionsButton.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
        }

        return true;
    }

	private void LoadTestRecords()
	{
		DateTime testDate = new DateTime(1, 1, 2013);
		for(int i = 0; i < 25; i++)
		{
            testRecords[i] = budget.createTransaction();
            testRecords[i].setAmount(50.0f);
            testRecords[i].setComment("Number " + i);
            testRecords[i].setCategoryName("Test Category");
			
			if( i % 3 == 0)
				testDate.changeDays(1);
            testRecords[i].setDate(new DateTime(testDate.getTimeInMilliseconds()));
            testRecords[i].setType(TransactionTypeEnum.CREDIT);
            if( i % 2 == 0)
                testRecords[i].setType(TransactionTypeEnum.WITHDRAWAL);
		}
		
		TransactionComparator byDate = new TransactionComparator();
		Arrays.sort(testRecords, byDate);
	}

    @Override
    public void eventFired() {
        this.checkToEnableDeleteButton();
    }

    private void checkToEnableDeleteButton() {

        if(this.listAdapter.isBoxesChecked()) {
            this.deleteTransactionsButton.setVisible(true);
        } else {
            this.deleteTransactionsButton.setVisible(false);
        }

    }

	class TransactionComparator implements Comparator<Transaction>
	{

		@Override
		public int compare(Transaction lhs, Transaction rhs) {
			int debug = DateTime.differenceBetweenDates(rhs.getDate(), lhs.getDate());
			return debug*-1;
		}
		
	}
}

package infrastructure.dao;

import android.util.Log;

import com.gwiz.androidutils.db.Database;

import infrastructure.data.CategoryDto;
import infrastructure.record.CategoryRecord;

/**
 * Created by Paul on 3/10/2015.
 */
public class CategoryDao extends Dao {

    CategoryRecord record;

    public CategoryDao(Database db) {
        super(db);
        this.record = new CategoryRecord();
    }

    public CategoryDto findCategory(int id) {
        this.record = new CategoryRecord();

        this.record.record.id = id;
        record.select(this.db, 1);

        if(!record.getGoodState()) {
            Log.e("UNABLE TO FIND RECORD", "Unable to find category with id: " + id);
            throw new RuntimeException("Unable to find category with id: " + id);
        }

        return record.record.clone();
    }

    public void saveCategory(CategoryDto dto) {
        this.record = new CategoryRecord();

        this.record.record.id = dto.id;
        record.select(this.db, 1);

        this.record.record = dto.clone();
        if(!record.getGoodState()) {
            this.record.insert(this.db);
        } else {
            this.record.update(this.db);
        }
    }

    public void deleteCategory(CategoryDto dto) {
        this.record = new CategoryRecord();

        this.record.record.id = dto.id;

        if( this.record.delete(this.db) < 1) {
            Log.e("UNABLE TO DELETE RECORD", "Unable to delete category with id: " + dto.id);
            throw new RuntimeException("Unable to delete category with id: " + dto.id);
        }
    }
}

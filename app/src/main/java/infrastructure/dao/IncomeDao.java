package infrastructure.dao;

import android.util.Log;
import com.gwiz.androidutils.db.Database;
import infrastructure.data.IncomeDto;
import infrastructure.record.IncomeRecord;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pgalasti on 2/2/16.
 */
public class IncomeDao extends Dao {

    private final static String TAG = "IncomeDao";

    private IncomeRecord incomeRecord;

    public IncomeDao(Database db) {
        super(db);

        this.incomeRecord = new IncomeRecord();
    }

    public boolean incomeExists(String id) {

        Log.d(TAG, "Checking if income with ID: " + id + " exists...");

        this.incomeRecord.initialize();
        this.incomeRecord.record.incomeGuid = id;

        this.incomeRecord.select(this.db, 1);
        if(this.incomeRecord.getGoodState()) {
            Log.d(TAG, "Income found.");
            return true;
        }

        Log.d(TAG, "Income not found.");
        return false;
    }

    public boolean findIncomeByName(String name) {

        Log.d(TAG, "Find Income with name: " + name + "...");

        this.incomeRecord.initialize();
        StringBuilder sb = new StringBuilder("SELECT * FROM")
                .append(this.incomeRecord.getRecordAttributes().getTableName())
                .append(" WHERE ").append(IncomeRecord.LABEL).append("='")
                .append(name).append("';");

        this.incomeRecord.select(this.db, sb.toString());
        if(this.incomeRecord.getGoodState()) {
            Log.d(TAG, "Income found.");
            return true;
        }

        Log.d(TAG, "Income not found.");
        return false;
    }

    public IncomeDto getIncome(String id) {

        Log.d(TAG, "Find Income with GUID: " + id + ".");

        this.incomeRecord.initialize();
        this.incomeRecord.record.incomeGuid = id;

        if(!this.incomeRecord.getGoodState()) {
            throw new RuntimeException("Unable to get Income with ID: " + id);
        }

        return this.incomeRecord.record.clone();
    }

    public List<IncomeDto> getAllIncomes() {

        Log.d(TAG, "Getting all Income records...");
        ;
        this.incomeRecord.initialize();
        incomeRecord.select(this.db, 0);

        List<IncomeDto> list = new ArrayList<>();

        if(!incomeRecord.getGoodState()) {
            Log.d(TAG, "No income records found!");
            return list;
        }

        do {
            list.add(incomeRecord.record.clone());
        }while(incomeRecord.readNext());

        Log.d(TAG, "Found " + list.size() + " records.");

        return list;
    }

}

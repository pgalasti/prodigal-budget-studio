package infrastructure.dao;

import android.util.Log;
import com.gwiz.androidutils.db.Database;

import java.util.ArrayList;
import java.util.List;

import infrastructure.data.BudgetDto;
import infrastructure.record.BudgetRecord;

/**
 * Created by Paul on 3/8/2015.
 */
public class BudgetDao extends Dao{

    private BudgetRecord budgetRecord;

    private final static String TAG = "BudgetDao";

    public BudgetDao(Database db) {
        super(db);
        this.budgetRecord = new BudgetRecord();
    }

    public boolean budgetExists(String id) {

        Log.d(TAG, "Checking if budget with ID: " + id + " exists...");

        budgetRecord.initialize();
        budgetRecord.record.budgetGuid = id;

        budgetRecord.select(this.db, 1);
        if(budgetRecord.getGoodState()) {
            Log.d(TAG, "Budget found.");
            return true;
        }

        Log.d(TAG, "Budget not found.");
        return false;
    }

    public boolean findBudgetName(String name) {

        Log.d(TAG, "Find Budget with name: " + name + "...");

        budgetRecord = new BudgetRecord();
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM ").append(budgetRecord.getRecordAttributes().getTableName())
                .append(" WHERE ").append(BudgetRecord.BUDGET_NAME).append("='").append(name).append("';");

        budgetRecord.select(this.db, sb.toString());
        if(budgetRecord.getGoodState()) {
            Log.d(TAG, "Budget found.");
            return true;
        }

        Log.d(TAG, "Budget not found.");
        return false;
    }

    public BudgetDto findBudget(String guid) {

        Log.d(TAG, "Find Budget with GUID: " + guid + ".");

        budgetRecord = new BudgetRecord();

        budgetRecord.record.budgetGuid = guid;
        budgetRecord.select(this.db, 1);

        return budgetRecord.record.clone();
    }

    public void saveBudget(BudgetDto dto) {

        Log.d(TAG, "Saving budget: " + dto.toStringDebug() + "...");

        this.budgetRecord.initialize();

        if(this.budgetExists(dto.budgetGuid)) {
            Log.d(TAG, "Updating budget: " + dto.budgetGuid + ".");
            this.budgetRecord.record = dto;
            this.budgetRecord.update(this.db);
            return;
        }

        Log.d(TAG, "Inserting budget: " + dto.budgetGuid + ".");
        this.budgetRecord.record = dto;
        this.budgetRecord.insert(this.db);
    }

    public void deleteBudget(String id) {

        Log.d(TAG, "Deleting budget: " + id + ".");

        this.budgetRecord.initialize();

        this.budgetRecord.record.budgetGuid = id;
        this.budgetRecord.delete(this.db);
    }

    public List<BudgetDto> getAllBudgets() {
        this.budgetRecord.initialize();
        List<BudgetDto> dtos = new ArrayList<>();

        this.budgetRecord.select(this.db, 0);

        if( this.budgetRecord.getGoodState() )
        {
            this.budgetRecord.getFirst();

            do {
                dtos.add(this.budgetRecord.record.clone());
            }while(this.budgetRecord.readNext());
        }

        return dtos;
    }

}

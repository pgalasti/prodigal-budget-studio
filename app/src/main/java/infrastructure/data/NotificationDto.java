package infrastructure.data;

import java.io.Serializable;

/**
 * Created by Paul on 3/8/2015.
 */
public class NotificationDto implements IDto, Serializable {

    public String	noteificationGuid;
    public boolean 	bReported;
    public String	notificationMessage;

    @Override
    public void initialize() {
        this.noteificationGuid = "";
        this.notificationMessage = "";
        this.bReported = false;
    }

    @Override
    public NotificationDto clone() {
        NotificationDto dto = new NotificationDto();
        dto.initialize();

        dto.noteificationGuid = this.noteificationGuid;
        dto.bReported = this.bReported;
        dto.notificationMessage = this.notificationMessage;

        return dto;
    }

    @Override
    public String toStringDebug() {

        StringBuilder sb = new StringBuilder();
        sb.append("GUID: ").append(this.noteificationGuid)
                .append(" Reported: ").append(this.bReported)
                .append(" Message: ").append(this.notificationMessage);

        return sb.toString();
    }
}

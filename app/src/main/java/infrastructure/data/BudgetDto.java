package infrastructure.data;

import java.io.Serializable;

/**
 * Created by Paul on 3/8/2015.
 */
public class BudgetDto implements IDto, Serializable  {

    public String 	budgetGuid;
    public String	budgetName;
    public float	limit;
    public float	current;

    @Override
    public void initialize() {
        budgetGuid = "";
        budgetName = "";
        current = limit = -0.0f;
    }

    @Override
    public BudgetDto clone() {
        BudgetDto dto = new BudgetDto();
        dto.initialize();;

        dto.budgetGuid = this.budgetGuid;
        dto.budgetName = this.budgetName;
        dto.limit = this.limit;
        dto.current = this.current;

        return dto;
    }

    @Override
    public String toStringDebug() {

        StringBuilder sb = new StringBuilder();
        sb.append("GUID: ").append(this.budgetGuid)
                .append(" Budget Name: ").append(this.budgetName)
                .append(" Limit: ").append(this.limit)
                .append(" Current: ").append(this.current);
        return sb.toString();
    }
}

package infrastructure.data;

import java.io.Serializable;

/**
 * Created by Paul on 3/10/2015.
 */
public class CategoryDto implements IDto, Serializable {

    public int id;
    public String categoryName;

    @Override
    public void initialize() {

        this.id = -1;
        this.categoryName = "";
    }

    @Override
    public CategoryDto clone() {
        CategoryDto dto = new CategoryDto();
        dto.id = this.id;
        dto.categoryName = this.categoryName;

        return dto;
    }

    @Override
    public String toStringDebug() {

        StringBuilder sb = new StringBuilder();
        sb.append("ID: ").append(this.id)
                .append(" Category Name: ").append(this.categoryName);

        return sb.toString();
    }
}

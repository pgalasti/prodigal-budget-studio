package infrastructure.data;

import java.io.Serializable;

/**
 * Created by Paul on 3/8/2015.
 */
public class TransactionTransferDto implements IDto, Serializable {

    public String transactionGuid;
    public String originGuid;

    @Override
    public void initialize() {
        this.transactionGuid = "";
        this.originGuid = "";
    }

    @Override
    public TransactionTransferDto clone() {
        TransactionTransferDto dto = new TransactionTransferDto();
        dto.initialize();

        dto.transactionGuid = this.transactionGuid;
        dto.originGuid = this.originGuid;

        return dto;
    }

    @Override
    public String toStringDebug() {

        StringBuilder sb = new StringBuilder();
        sb.append("GUID: ").append(this.transactionGuid)
                .append(" Origin GUID: ").append(this.originGuid);


        return sb.toString();
    }
}

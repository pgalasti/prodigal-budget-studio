package infrastructure.data;

/**
 * Created by Paul on 3/8/2015.
 */
public interface IDto {

    void initialize();

    IDto clone();

    String toStringDebug();
}

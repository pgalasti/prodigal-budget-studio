package infrastructure.data;

import com.gwiz.androidutils.util.DateTime;

import java.io.Serializable;

/**
 * Created by pgalasti on 1/27/16.
 */
public class IncomeDto implements IDto, Serializable {

    public String incomeGuid;
    public String label;
    public float amount;
    public char occurrenceInd;
    public char scheduledInd;
    public DateTime date;

    public IncomeDto() {
        this.initialize();
    }


    @Override
    public void initialize() {
        this.incomeGuid = "";
        this.label = "";
        this.amount = 0.0f;
        this.occurrenceInd = ' ';
        scheduledInd = ' ';
        date = new DateTime(0);
    }

    @Override
    public IncomeDto clone() {
        IncomeDto dto = new IncomeDto();
        dto.initialize();

        dto.incomeGuid = this.incomeGuid;
        dto.label = this.label;
        dto.amount = this.amount;
        dto.occurrenceInd = this.occurrenceInd;
        dto.scheduledInd = this.scheduledInd;
        dto.date = new DateTime(this.date.getTimeInMilliseconds());

        return dto;
    }

    @Override
    public String toStringDebug() {

        StringBuilder sb = new StringBuilder();
        sb.append("GUID: ").append(this.incomeGuid)
                .append(" Label: ").append(this.label)
                .append(" Amount: ").append(this.amount)
                .append(" Occurrence Indicator: ").append(this.occurrenceInd)
                .append(" Schedule Indicator: ").append(this.scheduledInd)
                .append(" Date: ").append(this.date.dateTimeToString(DateTime.DDMMCCYY));
        return sb.toString();
    }
}

package infrastructure.data;

import com.gwiz.androidutils.util.DateTime;

import java.io.Serializable;

/**
 * Created by pgalasti on 2/7/16.
 */
public class IncomePieceDto implements IDto, Serializable {

    public DateTime monthStartDate; // Key
    public DateTime pieceDate; // Key
    public String incomeGuid; // Income Guid; Key
    public String currentIncomeLabel;
    public float amount;
    public char occurrenceInd;

    @Override
    public void initialize() {
        monthStartDate = new DateTime(0);
        this.pieceDate = new DateTime(0);
        this.incomeGuid = "";
        this.currentIncomeLabel = "";
        this.amount = 0.0f;
        this.occurrenceInd = ' ';
    }

    @Override
    public IncomePieceDto clone() {

        IncomePieceDto dto = new IncomePieceDto();
        dto.monthStartDate = new DateTime(this.monthStartDate.getTimeInMilliseconds());
        dto.pieceDate = new DateTime(this.pieceDate.getTimeInMilliseconds());
        dto.incomeGuid = this.incomeGuid;

        dto.currentIncomeLabel = this.currentIncomeLabel;
        dto.amount = this.amount;
        dto.occurrenceInd = this.occurrenceInd;

        return dto;
    }

    @Override
    public String toStringDebug() {

        StringBuilder sb = new StringBuilder();
        sb.append("Month Start Date: ").append(this.monthStartDate.dateToString(DateTime.MMDDCCYY))
                .append(" Piece Date: ").append(this.pieceDate.dateToString(DateTime.MMDDCCYY))
                .append(" Current Income Guid: ").append(this.incomeGuid)
                .append(" Current Income Label: ").append(this.currentIncomeLabel)
                .append(" Piece Amount: ").append(this.amount)
                .append(" Occurrence Ind: ").append(this.occurrenceInd);
        return sb.toString();
    }
}

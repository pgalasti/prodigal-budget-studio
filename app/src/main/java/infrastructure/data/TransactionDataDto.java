package infrastructure.data;

import java.io.Serializable;

/**
 * Created by Paul on 3/8/2015.
 */
public class TransactionDataDto implements IDto, Serializable {

    public String transactionGuid;
    public long picId;

    @Override
    public void initialize() {
        transactionGuid = "";
        picId = 0L;
    }

    @Override
    public TransactionDataDto clone() {
        TransactionDataDto dto = new TransactionDataDto();
        dto.initialize();

        dto.transactionGuid = this.transactionGuid;
        dto.picId = this.picId;

        return dto;
    }

    @Override
    public String toStringDebug() {

        StringBuilder sb = new StringBuilder();
        sb.append("GUID: ").append(this.transactionGuid)
                .append(" Picture ID: ").append(this.picId);

        return sb.toString();
    }
}

package infrastructure.data;

import com.gwiz.androidutils.util.DateTime;

import java.io.Serializable;

/**
 * Created by Paul on 3/8/2015.
 */
public class TransactionDto implements IDto, Serializable {

    public String 	transGuid;
    public String 	budgetGuid;
    public char		type;
    public float	amount;
    public String	comment;
    public DateTime date;

    @Override
    public void initialize() {
        transGuid = "";
        this.budgetGuid = "";
        this.type = ' ';
        this.amount = 0.0f;
        this.comment = "";
        this.date = new DateTime();
    }

    @Override
    public TransactionDto clone() {
        TransactionDto dto = new TransactionDto();
        dto.initialize();

        dto.transGuid = this.transGuid;
        dto.budgetGuid = this.budgetGuid;
        dto.type = this.type;
        dto.amount = this.amount;
        dto.comment = this.comment;
        dto.date = this.date;

        return dto;
    }

    @Override
    public String toStringDebug() {

        StringBuilder sb = new StringBuilder();
        sb.append("GUID: ").append(this.transGuid)
                .append(" Budget GUID: ").append(this.budgetGuid)
                .append(" Type: ").append(this.type)
                .append(" Amount: ").append(this.amount)
                .append(" Comment: ").append(this.comment)
                .append(" Time: ").append(this.date.dateTimeToString(DateTime.MMDDCCYY | DateTime.HHMMSS));

        return sb.toString();
    }
}

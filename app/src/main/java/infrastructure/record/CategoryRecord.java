package infrastructure.record;

import android.content.ContentValues;

import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;

import infrastructure.data.CategoryDto;

/**
 * Created by Paul on 3/10/2015.
 */
public class CategoryRecord extends ProdigalRecord<CategoryDto> {

    public static final String CATEGORY_TABLE = "category";
    public static final String CATEGORY_ID = "id";
    public static final String CATEGORY_NAME = "name";

    @Override
    public CategoryDto createDto() {
        return new CategoryDto();
    }

    @Override
    protected void BindData() {
        short index = 0;
        record.id = m_Cursor.getInt(index++);
        record.categoryName = m_Cursor.getString(index++);
    }

    protected static IRecordAttributes attributes;
    static 	{
        attributes =  new IRecordAttributes() {

            @Override
            public String getTableName() {
                return CATEGORY_TABLE;
            }

            @Override
            public int getTableColumns() {
                return 2;
            }

            @Override
            public String getSQLCreateStatement() {
                StringBuilder SB = new StringBuilder();
                SB.append("CREATE TABLE ").append(getTableName()).append("(")
                        .append(CATEGORY_ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT, ")
                        .append(CATEGORY_NAME).append(" TEXT NOT NULL);");

                return SB.toString();
            }

            @Override
            public String getSQLAlterSchema(int oldVersion, int newVersion) {
                return "";
            }
        };
    }

    @Override
    public IRecordAttributes getRecordAttributes() {
        return attributes;
    }

    @Override
    public String getDebugDetail() {
        StringBuilder SB = new StringBuilder();
        SB.append("Table:").append(attributes.getTableName()).append("\n")
                .append("Columns:").append(attributes.getTableColumns()).append("\n")
                .append(CATEGORY_ID).append(":").append("INTEGER:").append(record.id).append("\n")
                .append(CATEGORY_NAME).append(":").append("TEXT:").append(record.categoryName).append("\n");
        return SB.toString();
    }

    @Override
    public void select(Database db, String sqlQuery) throws DBException {
        m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
        this.getFirst();
    }

    @Override
    public void select(Database db, int keys) throws DBException {
        if( keys < 1 )
        {
            m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
            this.getFirst();
            return;
        }

        StringBuilder sbSelection = new StringBuilder(CATEGORY_ID+"=?");
        String[] selecetionArgs = new String[keys];
        selecetionArgs[0] = String.valueOf(record.id);
        if( keys > 1 )
        {
            sbSelection.append(" AND " + CATEGORY_NAME + "=?");
            selecetionArgs[1] = record.categoryName;
        }

        m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
        this.getFirst();
    }

    @Override
    public int update(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put(CATEGORY_NAME, record.categoryName);

        return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, CATEGORY_ID+"=?", new String[]{String.valueOf(record.id)});
    }

    @Override
    public long insert(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put(CATEGORY_NAME, record.categoryName);

        long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
        if( insertID == -1 )
            throw new DBException("Failure inserting record.", this);

        return insertID;
    }

    @Override
    public int delete(Database db) throws DBException {
        return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), CATEGORY_ID+"=?", new String[]{String.valueOf(record.id)});
    }
}

package infrastructure.record;

import android.content.ContentValues;

import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;

import infrastructure.data.TransactionDataDto;

/**
 * Created by Paul on 3/8/2015.
 */
public class TransactionDataRecord extends ProdigalRecord<TransactionDataDto> {


    @Override
    public TransactionDataDto createDto() {
        return new TransactionDataDto();
    }

    @Override
    public void select(Database db, String sqlQuery) throws DBException {
        m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
        this.getFirst();
    }

    @Override
    public void select(Database db, int keys) throws DBException {
        if( keys < 1 )
        {
            m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
            this.getFirst();
            return;
        }

        StringBuilder sbSelection = new StringBuilder("trans_guid=?");
        String[] selecetionArgs = new String[keys];
        selecetionArgs[0] = record.transactionGuid;
        if( keys > 1 )
        {
            sbSelection.append(" AND pic_id=?");
            selecetionArgs[1] = String.valueOf(record.picId);
        }
    }

    @Override
    public int update(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put("pic_id", record.picId);

        return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, "trans_guid=?", new String[]{record.transactionGuid});
    }

    @Override
    public long insert(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put("trans_guid", record.transactionGuid);
        cv.put("pic_id", record.picId);

        long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
        if( insertID == -1 ) {
            throw new DBException("Failure inserting record.", this);
        }

        return insertID;
    }

    @Override
    public int delete(Database db) throws DBException {
        return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), "trans_guid=?", new String[]{record.transactionGuid});
    }

    @Override
    protected void BindData() {
        short index = 0;
        record.transactionGuid = m_Cursor.getString(index++);
        record.picId = m_Cursor.getLong(index++);
    }

    protected static IRecordAttributes attributes;
    static {
        attributes = new IRecordAttributes() {

            @Override
            public String getTableName() {
                return "trans_data";
            }

            @Override
            public int getTableColumns() {
                return 2;
            }

            @Override
            public String getSQLCreateStatement() {
                StringBuilder SB = new StringBuilder();
                SB.append("CREATE TABLE ").append(getTableName()).append("(")
                        .append("trans_guid TEXT PRIMARY KEY, ")
                        .append("pic_id LONG); ");

                return SB.toString();
            }

            @Override
            public String getSQLAlterSchema(int oldVersion, int newVersion) {
                return "";
            }
        };
    }

    @Override
    public IRecordAttributes getRecordAttributes() {
        return TransactionDataRecord.attributes;
    }

    @Override
    public String getDebugDetail() {
        StringBuilder SB = new StringBuilder();
        SB.append("Table:").append(attributes.getTableName()).append("\n")
                .append("Columns:").append(attributes.getTableColumns()).append("\n")
                .append("trans_guid:").append("TEXT:").append(record.transactionGuid).append("\n")
                .append("pic_id:").append("LONG:").append(record.picId).append("\n");
        return SB.toString();
    }
}

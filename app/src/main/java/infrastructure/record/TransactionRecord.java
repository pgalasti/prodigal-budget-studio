package infrastructure.record;

import android.content.ContentValues;

import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;

import infrastructure.data.TransactionDto;

public class TransactionRecord extends ProdigalRecord<TransactionDto> {

    @Override
    public TransactionDto createDto() {
        return new TransactionDto();
    }

	@Override
	public int update(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("budget_guid", record.budgetGuid);
		cv.put("type", Character.toString(record.type));
		cv.put("amount", record.amount);
		cv.put("comment", record.comment);
		cv.put("dt", record.date.getTimeInMilliseconds());

		return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, "trans_guid=?", new String[]{record.transGuid});
	}

	@Override
	public long insert(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("trans_guid", record.transGuid);
		cv.put("budget_guid", record.budgetGuid);
		cv.put("type", Character.toString(record.type));
		cv.put("amount", record.amount);
		cv.put("comment", record.comment);
		cv.put("dt", record.date.getTimeInMilliseconds());

		long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
		if( insertID == -1 )
			throw new DBException("Failure inserting record.", this);
		
		return insertID;
	}

	@Override
	public int delete(Database db) throws DBException {
		return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), "trans_guid=?", new String[]{record.transGuid});
	}

	@Override
	public void select(Database db, String sqlQuery) throws DBException {
		m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
		this.getFirst();
	} 

	@Override
	public void select(Database db, int keys) throws DBException {
		if( keys < 1 )
		{
			m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
			this.getFirst();
			return; 
		} 
		
		StringBuilder sbSelection = new StringBuilder("trans_guid=?");
		String[] selecetionArgs = new String[keys];
		selecetionArgs[0] = record.transGuid;
		if( keys > 1 )
		{
			sbSelection.append(" AND budget_guid=?");
			selecetionArgs[1] = record.budgetGuid;
		}
		if( keys > 2 )
		{
			sbSelection.append(" AND type=?");
			selecetionArgs[2] = String.valueOf(record.type);
		}
		if( keys > 3 )
		{
			sbSelection.append(" AND amount=?");
			selecetionArgs[3] = String.valueOf(record.amount);
		}
		if( keys > 4 )
		{
			sbSelection.append(" AND comment=?");
			selecetionArgs[4] = record.comment;
		}
		if( keys > 5 )
		{
			sbSelection.append(" AND dt=?");
			selecetionArgs[5] = String.valueOf(record.date);
		}

		m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
		this.getFirst();
	}

	@Override
	protected void BindData() {
		short index = 0;
        record.transGuid = m_Cursor.getString(index++);
        record.budgetGuid = m_Cursor.getString(index++);
        record.type = m_Cursor.getString(index++).charAt(0);
        record.amount = m_Cursor.getFloat(index++);
        record.comment = m_Cursor.getString(index++);
        record.date.setDateTime(m_Cursor.getLong(index++));
	}

	protected static IRecordAttributes attributes;
	static {
		attributes = new IRecordAttributes() {

			@Override
			public String getTableName() {
				return "trans";
			}

			@Override
			public int getTableColumns() {
				return 6;
			}

			@Override
			public String getSQLCreateStatement() {
				StringBuilder SB = new StringBuilder();
				SB.append("CREATE TABLE ").append(getTableName()).append("(")
				.append("trans_guid TEXT PRIMARY KEY, ")
				.append("budget_guid TEXT, ")
				.append("type CHARACTER(1), ")
				.append("amount FLOAT, ")
				.append("comment TEXT, ")
				.append("dt LONG); ");

				return SB.toString();
			}

			@Override
			public String getSQLAlterSchema(int oldVersion, int newVersion) {
				return "";
			}
		};
	}

	@Override
	public IRecordAttributes getRecordAttributes() {
		return attributes;
	}

	@Override
	public String getDebugDetail() {
		StringBuilder SB = new StringBuilder();
		SB.append("Table:").append(attributes.getTableName()).append("\n")
		.append("Columns:").append(attributes.getTableColumns()).append("\n")
		.append("trans_guid:").append("TEXT:").append(record.transGuid).append("\n")
		.append("type:").append("CHARACTER:").append(record.type).append("\n")
		.append("amount:").append("FLOAT:").append(record.amount).append("\n")
		.append("comment:").append("TEXT:").append(record.comment).append("\n")
		.append("dt:").append("LONG:").append(record.date).append("\n");
		return SB.toString();
	}

}

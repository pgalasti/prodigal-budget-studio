package infrastructure.record;

import android.content.ContentValues;
import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;
import com.gwiz.androidutils.util.DateTime;
import infrastructure.data.IncomePieceDto;

/**
 * Created by pgalasti on 2/7/16.
 */
public class IncomePieceRecord extends ProdigalRecord<IncomePieceDto> {

    public static final String INCOME_PIECE_TABLE = "income_piece";
    public static final String MONTH_START_DATE = "month_start_date";
    public static final String PIECE_DATE = "piece_date";
    public static final String INCOME_GUID = "income_guid";
    public static final String SAVED_LABEL = "saved_label";
    public static final String SAVED_AMOUNT = "saved_amount";
    public static final String OCCURRENCE_INDICATOR = "occ_ind";

    @Override
    public IncomePieceDto createDto() {
        return new IncomePieceDto();
    }

    @Override
    public void select(Database db, String sqlQuery) throws DBException {
        m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
        this.getFirst();
    }

    @Override
    public void select(Database db, int keys) throws DBException {

        if( keys < 1 )
        {
            m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
            this.getFirst();
            return;
        }

        StringBuilder sbSelection = new StringBuilder(MONTH_START_DATE).append("=?");
        String[] selecetionArgs = new String[keys];
        selecetionArgs[0] = String.valueOf(record.monthStartDate.getTimeInMilliseconds());
        if( keys > 1 ) {
            sbSelection.append(" AND ").append(PIECE_DATE).append("=?");
            selecetionArgs[1] = String.valueOf(record.pieceDate.getTimeInMilliseconds());
        }
        if( keys > 2 ) {
            sbSelection.append(" AND ").append(INCOME_GUID).append("=?");
            selecetionArgs[2] = String.valueOf(record.incomeGuid);
        }
        if( keys > 3 ) {
            sbSelection.append(" AND ").append(SAVED_LABEL).append("=?");
            selecetionArgs[3] = record.currentIncomeLabel;
        }
        if( keys > 4 ) {
            sbSelection.append(" AND ").append(SAVED_AMOUNT).append("=?");
            selecetionArgs[4] = String.valueOf(record.amount);
        }
        if( keys > 5 ) {
            sbSelection.append(" AND ").append(OCCURRENCE_INDICATOR).append("=?");
            selecetionArgs[5] = String.valueOf(record.occurrenceInd);
        }

        m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
        this.getFirst();
    }

    @Override
    public int update(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put(SAVED_LABEL, record.currentIncomeLabel);
        cv.put(SAVED_AMOUNT, record.amount);
        cv.put(OCCURRENCE_INDICATOR, String.valueOf(record.occurrenceInd));

        return db.getWritableDatabase().update(
                getRecordAttributes().getTableName(),
                cv,
                MONTH_START_DATE+"=? AND " +PIECE_DATE+"=? AND "+INCOME_GUID+"=?",
                new String[] {
                                String.valueOf(record.monthStartDate.getTimeInMilliseconds()),
                                String.valueOf(record.pieceDate.getTimeInMilliseconds()),
                                record.incomeGuid
                        });
    }

    @Override
    public long insert(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put(MONTH_START_DATE, String.valueOf(record.monthStartDate.getTimeInMilliseconds()));
        cv.put(PIECE_DATE, String.valueOf(record.pieceDate.getTimeInMilliseconds()));
        cv.put(INCOME_GUID, record.incomeGuid);
        cv.put(SAVED_LABEL, record.currentIncomeLabel);
        cv.put(SAVED_AMOUNT, record.amount);
        cv.put(OCCURRENCE_INDICATOR, String.valueOf(record.occurrenceInd));

        long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
        if( insertID == -1 )
            throw new DBException("Failure inserting record.", this);

        return insertID;
    }

    @Override
    public int delete(Database db) throws DBException {
        return db.getWritableDatabase().delete(
                getRecordAttributes().getTableName(),
                MONTH_START_DATE+"=? AND " +PIECE_DATE+"=? AND "+INCOME_GUID+"=?",
                new String[] {
                        String.valueOf(record.monthStartDate.getTimeInMilliseconds()),
                        String.valueOf(record.pieceDate.getTimeInMilliseconds()),
                        record.incomeGuid
                });
    }

    protected static IRecordAttributes attributes;
    static 	{
        attributes =  new IRecordAttributes() {

            @Override
            public String getTableName() {
                return INCOME_PIECE_TABLE;
            }

            @Override
            public int getTableColumns() {
                return 6;
            }

            @Override
            public String getSQLCreateStatement() {
                StringBuilder SB = new StringBuilder();

                SB.append("CREATE TABLE ").append(getTableName()).append("(")
                        .append(MONTH_START_DATE).append(" LONG, ")
                        .append(PIECE_DATE).append(" LONG, ")
                        .append(INCOME_GUID).append(" TEXT, ")
                        .append(SAVED_LABEL).append(" TEXT, ")
                        .append(SAVED_AMOUNT).append(" FLOAT, ")
                        .append(OCCURRENCE_INDICATOR).append(" TEXT, ")
                        .append("PRIMARY KEY (" + MONTH_START_DATE + ", " + PIECE_DATE + "," + INCOME_GUID + ") ); ");

                return SB.toString();
            }

            @Override
            public String getSQLAlterSchema(int oldVersion, int newVersion) {
                return "";
            }
        };
    }
    @Override
    public IRecordAttributes getRecordAttributes() {
        return attributes;
    }

    @Override
    public String getDebugDetail() {
        return null;
    }

    @Override
    protected void BindData() {

        short index = 0;
        record.monthStartDate = new DateTime(m_Cursor.getLong(index++));
        record.pieceDate = new DateTime(m_Cursor.getLong(index++));
        record.incomeGuid = m_Cursor.getString(index++);
        record.currentIncomeLabel = m_Cursor.getString(index++);
        record.amount = m_Cursor.getFloat(index++);
        record.occurrenceInd = m_Cursor.getString(index++).charAt(0);

    }
}

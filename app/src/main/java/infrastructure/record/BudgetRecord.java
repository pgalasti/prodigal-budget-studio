package infrastructure.record;

import android.content.ContentValues;

import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;

import infrastructure.data.BudgetDto;

public class BudgetRecord extends ProdigalRecord<BudgetDto> {

    public static final String BUDGET_TABLE = "budget";
    public static final String BUDGET_GUID = "budget_guid";
    public static final String BUDGET_NAME = "name";
    public static final String LIMIT = "lim";
    public static final String CURRENT = "current";

    @Override
    public BudgetDto createDto() {
        return new BudgetDto();
    }

	@Override
	public int update(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put(BUDGET_NAME, record.budgetName);
		cv.put(LIMIT, record.limit);
		cv.put(CURRENT, record.current);
		
		return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, BUDGET_GUID+"=?", new String[]{record.budgetGuid});
	}

	@Override
	public long insert(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put(BUDGET_GUID, record.budgetGuid);
        cv.put(BUDGET_NAME, record.budgetName);
        cv.put(LIMIT, record.limit);
        cv.put(CURRENT, record.current);
		
		long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
		if( insertID == -1 )
			throw new DBException("Failure inserting record.", this);
		
		return insertID;
	}

	@Override
	public int delete(Database db) throws DBException {
		return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), BUDGET_GUID+"=?", new String[]{record.budgetGuid});
	}

	@Override
	public void select(Database db, String sqlQuery) throws DBException {
		m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
		this.getFirst();
	}

	@Override
	public void select(Database db, int keys) throws DBException {
		if( keys < 1 )
		{
			m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
			this.getFirst();
			return;
		}
		
		StringBuilder sbSelection = new StringBuilder(BUDGET_GUID+"=?");
		String[] selecetionArgs = new String[keys];
		selecetionArgs[0] = record.budgetGuid;
		if( keys > 1 )
		{
			sbSelection.append(" AND " + BUDGET_NAME + "=?");
			selecetionArgs[1] = record.budgetName;
		}
		if( keys > 2 )
		{
			sbSelection.append(" AND "+ LIMIT + "+=?");
			selecetionArgs[2] = String.valueOf(record.limit);
		}
		if( keys > 3 )
		{
			sbSelection.append(" AND current "+ CURRENT + "+=?");
			selecetionArgs[3] = String.valueOf(record.current);
		}
		
		m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
		this.getFirst();
		
	}

	@Override
	protected void BindData() {
		short index = 0;
        record.budgetGuid = m_Cursor.getString(index++);
        record.budgetName = m_Cursor.getString(index++);
        record.limit = m_Cursor.getFloat(index++);
        record.current = m_Cursor.getFloat(index++);
	}

	protected static IRecordAttributes attributes;
	static 	{
		attributes =  new IRecordAttributes() {

			@Override
			public String getTableName() {
				return BUDGET_TABLE;
			}

			@Override
			public int getTableColumns() {
				return 4;
			}

			@Override
			public String getSQLCreateStatement() {
				StringBuilder SB = new StringBuilder();
				SB.append("CREATE TABLE ").append(getTableName()).append("(")
				.append(BUDGET_GUID).append(" TEXT PRIMARY KEY, ")
                .append(BUDGET_NAME).append(" TEXT, ")
                .append(LIMIT).append(" FLOAT, ")
                .append(CURRENT).append(" FLOAT); ");

				return SB.toString();
			}

			@Override
			public String getSQLAlterSchema(int oldVersion, int newVersion) {
				return "";
			}
		};
	}
	
	@Override
	public IRecordAttributes getRecordAttributes() {
		return attributes;
	}

	@Override
    public String getDebugDetail() {
        StringBuilder SB = new StringBuilder();
        SB.append("Table:").append(attributes.getTableName()).append("\n")
                .append("Columns:").append(attributes.getTableColumns()).append("\n")
                .append(BUDGET_GUID).append(":").append("TEXT:").append(record.budgetGuid).append("\n")
                .append(BUDGET_NAME).append(":").append("TEXT:").append(record.budgetName).append("\n")
                .append(LIMIT).append(":").append("FLOAT:").append(record.limit).append("\n")
                .append(CURRENT).append(":").append("FLOAT:").append(record.current).append("\n");
        return SB.toString();
	}
}

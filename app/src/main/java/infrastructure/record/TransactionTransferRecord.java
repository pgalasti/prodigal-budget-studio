package infrastructure.record;

import android.content.ContentValues;

import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;

import infrastructure.data.TransactionTransferDto;

/**
 * Created by Paul on 3/8/2015.
 */
public class TransactionTransferRecord extends ProdigalRecord<TransactionTransferDto> {

    @Override
    public TransactionTransferDto createDto() {
        return new TransactionTransferDto();
    }

    @Override
    public void select(Database db, String sqlQuery) throws DBException {
        m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
        this.getFirst();
    }

    @Override
    public void select(Database db, int keys) throws DBException {

        if( keys < 1 )
        {
            m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
            this.getFirst();
            return;
        }

        StringBuilder sbSelection = new StringBuilder("trans_guid=?");
        String[] selecetionArgs = new String[keys];
        selecetionArgs[0] = record.transactionGuid;
        if( keys > 1 )
        {
            sbSelection.append(" AND origin_guid=?");
            selecetionArgs[1] = record.originGuid;
        }
        m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
        this.getFirst();
    }

    @Override
    public int update(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put("origin_guid", record.originGuid);

        return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, "trans_guid=?", new String[]{record.transactionGuid});
    }

    @Override
    public long insert(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put("trans_guid", record.transactionGuid);
        cv.put("origin_guid", record.originGuid);

        long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
        if( insertID == -1 ) {
            throw new DBException("Failure inserting record.", this);
        }

        return insertID;
    }

    @Override
    public int delete(Database db) throws DBException {
        return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), "trans_guid=?", new String[]{record.transactionGuid});
    }

    @Override
    protected void BindData() {
        short index = 0;
        record.transactionGuid = m_Cursor.getString(index++);
        record.originGuid = m_Cursor.getString(index++);
    }

    protected static IRecordAttributes attributes;
    static 	{
        attributes =  new IRecordAttributes() {

            @Override
            public String getTableName() {
                return "trans_transfer";
            }

            @Override
            public int getTableColumns() {
                return 2;
            }

            @Override
            public String getSQLCreateStatement() {
                StringBuilder SB = new StringBuilder();
                SB.append("CREATE TABLE ").append(getTableName()).append("(")
                        .append("trans_guid TEXT, ")
                        .append("origin_guid TEXT, ")
                        .append("type LONG, ")
                        .append("PRIMARY KEY (trans_guid, origin_guid));");

                return SB.toString();
            }

            @Override
            public String getSQLAlterSchema(int oldVersion, int newVersion) {
                return "";
            }
        };
    }
    @Override
    public IRecordAttributes getRecordAttributes() {
        return TransactionTransferRecord.attributes;
    }

    @Override
    public String getDebugDetail() {
        StringBuilder SB = new StringBuilder();
        SB.append("Table:").append(attributes.getTableName()).append("\n")
                .append("Columns:").append(attributes.getTableColumns()).append("\n")
                .append("trans_guid:").append("TEXT:").append(record.transactionGuid).append("\n")
                .append("origin_guid:").append("TEXT:").append(record.originGuid).append("\n");
        return SB.toString();
    }
}

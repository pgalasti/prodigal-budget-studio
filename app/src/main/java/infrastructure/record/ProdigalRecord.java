package infrastructure.record;

import com.gwiz.androidutils.db.AQueryRecord;

import infrastructure.data.IDto;

/**
 * Created by Paul on 3/8/2015.
 */
public abstract class ProdigalRecord<DtoRecord extends IDto> extends AQueryRecord{

    public DtoRecord record;

    public ProdigalRecord() {
        this.record = createDto();
        this.initialize();
    }

    public void initialize() {
        this.record.initialize();
    }

    public abstract DtoRecord createDto();

}

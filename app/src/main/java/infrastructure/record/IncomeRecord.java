package infrastructure.record;

import android.content.ContentValues;
import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;
import com.gwiz.androidutils.util.DateTime;
import infrastructure.data.IncomeDto;

/**
 * Created by pgalasti on 1/27/16.
 */
public class IncomeRecord extends ProdigalRecord<IncomeDto> {

    public static final String INCOME_TABLE = "income";
    public static final String INCOME_GUID = "income_guid";
    public static final String LABEL = "label";
    public static final String AMOUNT = "amount";
    public static final String OCCURRENCE_INDICATOR = "occ_ind";
    public static final String SCHEDULE_INDICATOR = "sched_ind";
    public static final String START_DATE = "start_date";

    public static final char OCCURRENCE_IND_DAILY = 'D';
    public static final char OCCURRENCE_IND_WEEKLY = 'W';
    public static final char OCCURRENCE_IND_BIWEEKLY = 'B';
    public static final char OCCURRENCE_IND_MONTHLY = 'M';
    public static final char OCCURRENCE_IND_BIMONTHLY = 'N';
    public static final char OCCURRENCE_IND_SPECIFIC = 'S';

    @Override
    public IncomeDto createDto() {
        return new IncomeDto();
    }

    @Override
    public int update(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put(LABEL, record.label);
        cv.put(AMOUNT, record.amount);
        cv.put(OCCURRENCE_INDICATOR, String.valueOf(record.occurrenceInd));
        cv.put(SCHEDULE_INDICATOR, String.valueOf(record.scheduledInd));
        cv.put(START_DATE, record.date.getTimeInMilliseconds());

        return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, INCOME_GUID+"=?", new String[]{record.incomeGuid});
    }

    @Override
    public long insert(Database db) throws DBException {
        ContentValues cv = new ContentValues();
        cv.put(INCOME_GUID, record.incomeGuid);
        cv.put(LABEL, record.label);
        cv.put(AMOUNT, record.amount);
        cv.put(OCCURRENCE_INDICATOR, String.valueOf(record.occurrenceInd));
        cv.put(SCHEDULE_INDICATOR, String.valueOf(record.scheduledInd));
        cv.put(START_DATE, record.date.getTimeInMilliseconds());

        long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
        if( insertID == -1 )
            throw new DBException("Failure inserting record.", this);

        return insertID;
    }

    @Override
    public int delete(Database db) throws DBException {
        return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), INCOME_GUID+"=?", new String[]{record.incomeGuid});
    }

    @Override
    public void select(Database db, String sqlQuery) throws DBException {
        m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
        this.getFirst();
    }

    @Override
    public void select(Database db, int keys) throws DBException {

        if( keys < 1 )
        {
            m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
            this.getFirst();
            return;
        }

        StringBuilder sbSelection = new StringBuilder(INCOME_GUID).append("=?");
        String[] selecetionArgs = new String[keys];
        selecetionArgs[0] = record.incomeGuid;
        if( keys > 1 )
        {
            sbSelection.append(" AND ").append(LABEL).append("=?");
            selecetionArgs[1] = record.label;
        }
        if( keys > 2 )
        {
            sbSelection.append(" AND ").append(AMOUNT).append("=?");
            selecetionArgs[2] = String.valueOf(record.amount);
        }
        if( keys > 3 )
        {
            sbSelection.append(" AND ").append(OCCURRENCE_INDICATOR).append("=?");
            selecetionArgs[3] = String.valueOf(record.occurrenceInd);
        }
        if( keys > 4 )
        {
            sbSelection.append(" AND ").append(SCHEDULE_INDICATOR).append("=?");
            selecetionArgs[4] = String.valueOf(record.scheduledInd);
        }
        if( keys > 5 )
        {
            sbSelection.append(" AND ").append(START_DATE).append("=?");
            selecetionArgs[4] = String.valueOf(record.date.getTimeInMilliseconds());
        }

        m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
        this.getFirst();
    }

    @Override
    protected void BindData() {
        short index = 0;
        record.incomeGuid = m_Cursor.getString(index++);
        record.label = m_Cursor.getString(index++);
        record.amount = m_Cursor.getFloat(index++);
        record.occurrenceInd = m_Cursor.getString(index++).charAt(0);
        record.scheduledInd = m_Cursor.getString(index++).charAt(0);
        record.date = new DateTime(m_Cursor.getLong(index++));
    }

    protected static IRecordAttributes attributes;
    static 	{
        attributes =  new IRecordAttributes() {

            @Override
            public String getTableName() {
                return INCOME_TABLE;
            }

            @Override
            public int getTableColumns() {
                return 6;
            }

            @Override
            public String getSQLCreateStatement() {
                StringBuilder SB = new StringBuilder();
                SB.append("CREATE TABLE ").append(getTableName()).append("(")
                        .append(INCOME_GUID).append(" TEXT PRIMARY KEY, ")
                        .append(LABEL).append(" TEXT, ")
                        .append(AMOUNT).append(" FLOAT, ")
                        .append(OCCURRENCE_INDICATOR).append(" TEXT, ")
                        .append(SCHEDULE_INDICATOR).append(" TEXT, ")
                        .append(START_DATE).append(" LONG); ");

                return SB.toString();
            }

            @Override
            public String getSQLAlterSchema(int oldVersion, int newVersion) {
                return "";
            }
        };
    }

    @Override
    public IRecordAttributes getRecordAttributes() {
        return attributes;
    }

    @Override
    public String getDebugDetail() {
        StringBuilder SB = new StringBuilder();
        SB.append("Table:").append(attributes.getTableName()).append("\n")
                .append("Columns:").append(attributes.getTableColumns()).append("\n")
                .append(INCOME_GUID).append(":").append("TEXT:").append(record.incomeGuid).append("\n")
                .append(LABEL).append(":").append("TEXT:").append(record.label).append("\n")
                .append(AMOUNT).append(":").append("FLOAT:").append(record.amount).append("\n")
                .append(OCCURRENCE_INDICATOR).append(":").append("TEXT:").append(record.occurrenceInd).append("\n")
                .append(SCHEDULE_INDICATOR).append(":").append("TEXT:").append(record.scheduledInd).append("\n")
                .append(START_DATE).append(":").append("LONG:").append(record.date.getTimeInMilliseconds()).append("\n");
        return SB.toString();
    }
}

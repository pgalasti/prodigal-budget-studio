package infrastructure.record;

import android.content.ContentValues;

import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;

import infrastructure.data.NotificationDto;

public class NotificationRecord extends ProdigalRecord<NotificationDto>{


    @Override
    public NotificationDto createDto() {
        return new NotificationDto();
    }

	@Override
	public int update(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("reported", record.bReported ? 1 : 0);
		cv.put("message", record.notificationMessage);
		
		return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, "note_guid=?", new String[]{record.noteificationGuid});
	}

	@Override
	public long insert(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("note_guid", record.noteificationGuid);
		cv.put("reported", record.bReported ? 1 : 0);
		cv.put("message", record.notificationMessage);
		
		
		long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
		if( insertID == -1 )
			throw new DBException("Error inserting record.", this);
		
		return insertID;
	}

	@Override
	public int delete(Database db) throws DBException {
		return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), "note_guid=?", new String[]{record.noteificationGuid});
	}

	@Override
	public void select(Database db, String sqlQuery) throws DBException {
		m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
		this.getFirst();
	}

	@Override
	public void select(Database db, int keys) throws DBException {
		if( keys < 1 )
		{
			m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
			this.getFirst();
			return;
		}
		
		StringBuilder sbSelection = new StringBuilder("note_guid=?");
		String[] selecetionArgs = new String[keys];
		selecetionArgs[0] = record.noteificationGuid;
		if( keys > 1 )
		{
			sbSelection.append(" AND reported=?");
			selecetionArgs[1] = record.bReported ? String.valueOf(1) : String.valueOf(0);
		}
		if( keys > 2 )
		{
			sbSelection.append(" AND message=?");
			selecetionArgs[2] = record.notificationMessage;
		}
		
		
		m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
		this.getFirst();
	}

	@Override
	protected void BindData() {
		short index = 0;
        record.noteificationGuid = m_Cursor.getString(index++);
        record.bReported = m_Cursor.getInt(index++) == 1 ? true : false;
        record.notificationMessage = m_Cursor.getString(index++);
	}

	protected static IRecordAttributes attributes;
	static 	{
		attributes =  new IRecordAttributes() {

			@Override
			public String getTableName() {
				return "notif";
			}

			@Override
			public int getTableColumns() {
				return 3;
			}

			@Override
			public String getSQLCreateStatement() {
				StringBuilder SB = new StringBuilder();
				SB.append("CREATE TABLE ").append(getTableName()).append("(")
				.append("note_guid TEXT PRIMARY KEY, ")
				.append("reported INTEGER, ")
				.append("message TEXT);");

				return SB.toString();
			}

			@Override
			public String getSQLAlterSchema(int oldVersion, int newVersion) {
				return "";
			}
		};
	}
	
	@Override
	public IRecordAttributes getRecordAttributes() {
		return attributes;
	}

	@Override
	public String getDebugDetail() {
		StringBuilder SB = new StringBuilder();
		SB.append("Table:").append(attributes.getTableName()).append("\n")
		.append("Columns:").append(attributes.getTableColumns()).append("\n")
		.append("note_guid:").append("TEXT:").append(record.noteificationGuid).append("\n")
		.append("reported:").append("INTEGER:").append(record.bReported).append("\n")
		.append("message:").append("TEXT:").append(record.notificationMessage).append("\n");
		return SB.toString();
	}

}

package infrastructure.db;

import android.content.Context;

import android.util.Log;
import com.gwiz.androidutils.db.Database;

import infrastructure.record.*;

public class ProdigalBudgetDatabase extends Database {

	private String dbName;

	private static final String TAG = "ProdigalBudgetDatabase";

	public ProdigalBudgetDatabase(Context context, String name, int version) {
		super(context, name, version);
		Log.d(TAG, "Creating prodigal database object.");
		dbName = name;
	}

	@Override
	protected void setViewList() {
		BudgetRecord budget = new BudgetRecord();
		NotificationRecord notif = new NotificationRecord();
		TransactionRecord trans = new TransactionRecord();
        TransactionDataRecord transData = new TransactionDataRecord();
        TransactionTransferRecord transTransfer = new TransactionTransferRecord();
        CategoryRecord category = new CategoryRecord();
		IncomeRecord income = new IncomeRecord();
		IncomePieceRecord incomePiece = new IncomePieceRecord();
		
		viewList.add(budget.getRecordAttributes());
		viewList.add(notif.getRecordAttributes());
		viewList.add(trans.getRecordAttributes());
        viewList.add(transData.getRecordAttributes());
        viewList.add(transTransfer.getRecordAttributes());
        viewList.add(category.getRecordAttributes());
		viewList.add(income.getRecordAttributes());
		viewList.add(incomePiece.getRecordAttributes());
	}

	@Override
	public String getDatabaseName()	{
		return dbName;
	}
	
	@Override
	public String getDBMSName() {
		return "SQLite3";
	}
	
	@Override
	public String getDBMSVersion()	{
		return "3"; // Not actually sure. Not important for this project though.
	}
}

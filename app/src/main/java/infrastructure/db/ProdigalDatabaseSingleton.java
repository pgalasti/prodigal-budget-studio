package infrastructure.db;

import android.content.Context;
import android.util.Log;

public class ProdigalDatabaseSingleton {

	private static ProdigalBudgetDatabase db;

	private static final String TAG = "ProdigalDatabaseSingleton";

	private ProdigalDatabaseSingleton() {}
	
	public static ProdigalBudgetDatabase getInstance(Context context) {
		if( db == null ) {
			Log.d(TAG, "DB instance is null; creating new instance.");
			db = new ProdigalBudgetDatabase(context, "prodigal.db", 1);
		}
		
		return db;
	}
}

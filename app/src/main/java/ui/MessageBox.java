/**
 * MessageBox.java
 * 
 * All rights and source belongs to Paul Galasti(pgalasti@gmail.com).
 * 
 * This file is associated with 'Prodigal Budget' Application for Android devices.
 */

package ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gwiz.prodigalbudget.R;

import domain.Notification;
import domain.NotificationRepo;
import infrastructure.record.NotificationRecord;
import infrastructure.db.ProdigalBudgetDatabase;
import infrastructure.db.ProdigalDatabaseSingleton;

/**
 * A view model for displaying a message box notification to the user. A {@link NotificationRecord}
 * can be loaded into the view. Embedded views include:
 * 		</br><li>{@link android.widget.TextView} of the notification text</li>
 * 		</br><li>{@link android.widget.ImageView} of the dismiss image</li>
 * </br></br>
 * 
 * <b>TODO Current logic resides in view to dismiss notification record from database. 
 * This should be delegated to a different system instead of the UI class.</b> 
 * @author pgalasti@gmail.com
 */
public class MessageBox extends LinearLayout {

	private Context context = null;
	
	private ImageView cancel = null;
	private TextView text = null;
	
	private Notification notification = null;
	private boolean isCleared = false;
	
	
	/**
	 * Creates a blank MessageBox view.
	 * Text is set blank.
	 * @param context The application {@link android.content.Context}.
	 */
	public MessageBox(Context context) {
		super(context);
		View.inflate(context, R.layout.info_box, null);

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.info_box, this, true);

		cancel = (ImageView)findViewById(R.id.info_cancel_notification);
		text = (TextView) findViewById(R.id.info_box_message);
		this.context = context;
	}
	
	/**
	 * Loads the contents of a {@link NotificationRecord} into the MessageBox view. Current
	 * model will dismiss the {@link NotificationRecord} from the database to not show up again.
	 * 
	 * @param context The application {@link android.content.Context}.
	 * @param notification {@link NotificationRecord} with contents to load into the view.
	 */
	public MessageBox(Context context, Notification notification) {
		this(context);
		this.notification = notification;
	}

	/**
	 * Default constructor, don't use.
	 * @param context
	 * @param attr
	 */
	public MessageBox(Context context, AttributeSet attr) {
		super(context, attr);
	}
	
	/**
	 * Returns the {@link android.widget.ImageView} of the cancel image button.
	 * @return The {@link android.widget.ImageView} of the cancel image button.
	 */
	public ImageView getCancelImage() {
		return cancel;
	}
	
	/**
	 * Sets the text of the MessageBox {@link android.widget.TextView}
	 * @param text The text of the MessageBox {@link android.widget.TextView}
	 */
	public void setText(String text) {
		this.text.setText(text);
		
		invalidate();
		requestLayout();
	}
	
	/**
	 * Sets the text of the MessageBox {@link android.widget.TextView} using the contents of a {@link NotificationRecord}
	 * @param notification The {@link NotificationRecord} with contents to populate.
	 */
	public void setText(Notification notification) {
		this.notification = notification;
		this.setText(notification.getMessage());
	}
	
	/**
	 * Starts the intro slide animiation for the view.
	 */
	public void startNotificationAnimation() {
		Animation rotateAnimation = AnimationUtils.loadAnimation(context,  R.anim.add_to_screen);
		this.startAnimation(rotateAnimation);
	}
	
	/**
	 * Starts the slide animiation for the view when dismissed.
	 * Also, current model will remove the {@link NotificationRecord} from the database.
	 */
	public void cancelNotification() {
		Animation clearAnimation = AnimationUtils.loadAnimation(context,  R.anim.move_off_screen);
		this.startAnimation(clearAnimation);
		isCleared = true;
        this.notification.setNotified(true);

		// TODO async this
		ProdigalBudgetDatabase db = ProdigalDatabaseSingleton.getInstance(context);
        NotificationRepo repo = new NotificationRepo(db);
        repo.delete(this.notification);
	}
	
	
	@Override
	protected void onAnimationEnd() {
		super.onAnimationEnd();
		if( isCleared ) {
			this.setVisibility(View.GONE);
			isCleared = false;
		}
		
		invalidate();
		requestLayout();
		super.onAnimationEnd(); // TODO Test this.
	} 
}

package ui;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gwiz.androidutils.util.CurrencyBuilder;
import com.gwiz.prodigalbudget.R;

import domain.Transaction;
import domain.TransactionTypeEnum;

public class TransactionItem extends LinearLayout {

	private Transaction transaction;
	
	private LinearLayout transactionColor;
	private CheckBox checkbox;
	private TextView commentText;
	private TextView categoryText;
	private TextView amountText;
	
	public TransactionItem(Context context) {
		super(context);
        this.initViews();
	}

    public TransactionItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.initViews();
    }

	public TransactionItem(Context context, Transaction transaction)
	{
		super(context);
        this.initViews();
        this.setTransaction(transaction);
	}

    public Transaction getTransaction() {
        return this.transaction;
    }

    public void setCheckBox() {
        this.checkbox.setEnabled(true);
    }

    public void setUnCheckBox() {
        this.checkbox.setEnabled(false);
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
        this.bindFields();
    }

    public boolean isCheckboxChecked() {
        return this.checkbox.isChecked();
    }

    protected CheckBox getCheckbox() {
        return this.checkbox;
    }

    private void initViews() {

        View.inflate(this.getContext(), R.layout.transaction_item, null);
        LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.transaction_item, this, true);

        this.transactionColor = (LinearLayout)findViewById(R.id.transaction_color_indicator);
        this.checkbox = (CheckBox)findViewById(R.id.transaction_item_check);
        this.commentText = (TextView)findViewById(R.id.transaction_item_comment);
        this.categoryText = (TextView)findViewById(R.id.transaction_item_category);
        this.amountText = (TextView)findViewById(R.id.transaction_item_amount);
    }

    private void bindFields() {

        // Color
        TransactionTypeEnum type = transaction.getType();
        switch(type) {
            case WITHDRAWAL:
                this.transactionColor.setBackgroundColor(Color.RED);
                break;
            case CREDIT:
                this.transactionColor.setBackgroundColor(Color.GREEN);
                break;
            case TRANSFER:
                this.transactionColor.setBackgroundColor(Color.YELLOW);
                break;
        };

        // Default checkbox to unset
        this.setCheckBox();

        // Comment
        this.commentText.setText(this.transaction.getComment());

        // Category Name
        this.categoryText.setText(this.transaction.getCategoryName());

        // Amount
        final String amount = CurrencyBuilder.getCurrencyWithSymbol(transaction.getAmount());
        this.amountText.setText(amount);
        if(transaction.getAmount() < 0.00f) {
            this.amountText.setTextColor(Color.RED);
        }
    }
}

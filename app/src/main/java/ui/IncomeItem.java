package ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.gwiz.androidutils.util.DateTime;
import com.gwiz.prodigalbudget.R;
import domain.Income;

/**
 * Created by pgalasti on 2/3/16.
 */
public class IncomeItem extends LinearLayout {

    protected Context context;

    protected TextView incomeNameTextView;
    protected TextView incomeAmountTextView;
    protected TextView incomeOccurrenceTextView;
    protected TextView incomeDateTextView;
    protected CheckBox receivedCheckBox;

    protected DateTime viewDate;

    public IncomeItem(Context context) {
        super(context);

        View.inflate(context, R.layout.income_item, null);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.income_item, this, true);

        /** Views here **/
        this.bindControls();

        /** Custom Logic Here **/
        this.context = context;
        this.viewDate = new DateTime();
    }

    public IncomeItem(Context context, final Income income, final DateTime date) {
        super(context);

        View.inflate(context, R.layout.income_item, null);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.income_item, this, true);
        this.context = context;

        /** Views here **/
        this.bindControls();

        /** Custom Logic Here **/
        this.context = context;
        this.viewDate = new DateTime(date.getTimeInMilliseconds());
        this.setIncome(income);
    }

    public IncomeItem(Context context, AttributeSet attr) {
        super(context, attr);
    }

    /**
     * Starts the slide animiation for the view with a defined delay in milliseconds.
     * @param delay The delay to start the slide animiation.
     */
    public void startIncomeItemSlideAnimation(int delay) {
        Animation translateAnimation = AnimationUtils.loadAnimation(this.context,  R.anim.add_to_screen_plain);
        translateAnimation.setStartOffset(delay);
        this.startAnimation(translateAnimation);
    }

    public void setIncome(final Income income) {

        this.incomeNameTextView.setText(income.getName());
        this.incomeAmountTextView.setText(String.valueOf(income.getAmount())); // Format currency
        this.incomeOccurrenceTextView.setText(income.getOccurrenceName());
        this.incomeDateTextView.setText(income.getDate().dateTimeToString(DateTime.MMDDCCYY));
        this.receivedCheckBox.setChecked(new DateTime().getTimeInMilliseconds() >= this.viewDate.getTimeInMilliseconds());

    }

    private void bindControls() {
        this.incomeNameTextView = (TextView)findViewById(R.id.income_item_name_text);
        this.incomeAmountTextView = (TextView)findViewById(R.id.income_item_amount_text);
        this.incomeOccurrenceTextView = (TextView)findViewById(R.id.income_item_occurrence_text);
        this.incomeDateTextView = (TextView)findViewById(R.id.income_item_date_text);
        this.receivedCheckBox = (CheckBox)findViewById(R.id.income_item_received_check_box);
    }

}

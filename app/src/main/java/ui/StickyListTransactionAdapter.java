package ui;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.gwiz.androidutils.ui.stickylistheaders.StickyListHeadersAdapter;
import com.gwiz.androidutils.util.DateTime;
import com.gwiz.prodigalbudget.R;

import java.util.ArrayList;
import java.util.List;

import domain.Transaction;

public class StickyListTransactionAdapter extends BaseAdapter implements StickyListHeadersAdapter, AdapterSignaler {

	private LayoutInflater inflater;	
	private Transaction[] transactions;

    private List<AdapterListener> listeners;

    private int checkedCount;

	public StickyListTransactionAdapter(Context context, Transaction[] transactions)
	{
		inflater = LayoutInflater.from(context);
        this.transactions = transactions;
        this.listeners = new ArrayList<>();
        this.checkedCount = 0;
	}
	
	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	@Override
	public boolean isEnabled(int position) { 
		return true;
	}

	@Override
	public int getCount() {
		return transactions.length;
	}

	@Override
	public Object getItem(int position) {
		return transactions[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
        return Adapter.IGNORE_ITEM_VIEW_TYPE;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

        Transaction transaction = transactions[position];
		TransactionItem item = new TransactionItem(parent.getContext(), transaction);

        item.getCheckbox().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox checkBox = (CheckBox)v;

                if(checkBox.isChecked()) {
                    ++checkedCount;
                } else {
                    --checkedCount;
                }

                for(AdapterListener listener : listeners) {
                    listener.eventFired();
                }
            }
        });

		return item;
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return getCount() == 0;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		
		convertView = inflater.inflate(R.layout.transaction_list_header,  parent, false);
		TextView dateHeader = (TextView)convertView.findViewById(R.id.transaction_header_text);
		DateTime dateTime = transactions[position].getDate();
		dateHeader.setText(dateTime.dateToString(DateTime.MMDDCCYY));
		convertView.setTag(dateHeader);
		return convertView;
	}

	@Override
	public long getHeaderId(int position) {
		return transactions[position].getDate().getTimeInMilliseconds();
	}

    @Override
    public void addListener(AdapterListener listener) {
        listeners.add(listener);
    }

    public boolean isBoxesChecked() {
        return checkedCount > 0;
    }

	
}

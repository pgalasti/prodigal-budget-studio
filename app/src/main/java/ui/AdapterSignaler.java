package ui;

public interface AdapterSignaler {
    public void addListener(AdapterListener listener);
}
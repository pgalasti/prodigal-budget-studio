package applogic;

import android.content.Context;
import android.util.Log;

public class InitializationSingleton {

	private volatile static InitializationManager manager;

	private static final String TAG = "InitializationSingleton";

	/** Construction not available outside singleton*/
	private InitializationSingleton() {}
	
	public static InitializationManager getInstance(Context context) {
		if( manager == null ) {
			Log.d(TAG, "Instance is null, now creating instance.");
			manager = new InitializationManager(context);
		}
		return manager;
	}
}
